import { WebSocketClient } from "@napalmbrain/websocket-manager";

import { GRAPHQL } from "../common";

export function initGraphQLClientHandlers(this: WebSocketClient) {
  this.handle(GRAPHQL.subscribe_ping, (connection, message) => {
    const action = connection.actions!.get(message.id as string);
    if (action) {
      action.timeout?.restart();
      this.send(connection, {
        id: message.id,
        type: GRAPHQL.subscribe_pong,
      });
    }
  });
  this.handle(GRAPHQL.subscribe_next, (connection, message) => {
    connection.actions!.get(message.id as string)?.observable.next(message);
  });
  this.handle(GRAPHQL.subscribe_error, (connection, message) => {
    connection.actions!.get(message.id as string)?.observable.error(message);
  });
  this.handle(GRAPHQL.subscribe_complete, (connection, message) => {
    connection.actions!.get(message.id as string)?.observable.complete();
  });
}
