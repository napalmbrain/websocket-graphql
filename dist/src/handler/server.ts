import {
  DocumentNode,
  execute,
  parse,
  subscribe,
  validate,
  OperationDefinitionNode,
  DefinitionNode,
  ExecutionResult,
  GraphQLError,
} from "graphql";

import {
  Generic,
  Signaler,
  Timeout,
  TIMEOUT,
  KEEPALIVE,
  WebSocketServer,
  random,
} from "@napalmbrain/websocket-manager";

import {
  GRAPHQL,
  GraphQLServerOptions,
  GraphQLConnection,
  Subscription,
  NO_RESPONSE,
} from "../common";
import { isFunction } from "../utils";

export function initGraphQLServerHandlers(
  this: WebSocketServer,
  options: GraphQLServerOptions
) {
  this.handle(
    GRAPHQL.subscribe_pong,
    (connection: GraphQLConnection, message) => {
      connection.subscriptions!.get(message.id as string)?.timeout.restart();
    }
  );
  this.handle(
    GRAPHQL.subscribe_cancel,
    (connection: GraphQLConnection, message) => {
      connection
        .subscriptions!.get(message.id as string)
        ?.cancel.signal({ reason: "canceled" });
    }
  );
  this.handle(
    GRAPHQL.subscribe_start,
    async (connection: GraphQLConnection, message) => {
      let document: DocumentNode;
      try {
        document = parse(message.query as string);
      } catch (error) {
        this.send(connection, {
          id: message.id,
          type: GRAPHQL.subscribe_error,
          errors: [(error as GraphQLError).toString()],
        });
        this.send(connection, {
          id: message.id,
          type: GRAPHQL.subscribe_complete,
        });
        return;
      }
      const validationErrors = validate(options.schema, document);
      if (validationErrors.length) {
        this.send(connection, {
          id: message.id,
          type: GRAPHQL.subscribe_error,
          errors: validationErrors,
        });
        this.send(connection, {
          id: message.id,
          type: GRAPHQL.subscribe_complete,
        });
        return;
      }
      let contextValue: Record<string, unknown> = {};
      const variableValues = message.variables as Record<string, unknown>;
      if (options.context) {
        if (isFunction(options.context)) {
          //@ts-ignore: `this.context` is callable here.
          const context = await options.context(context);
          if (context) {
            contextValue = context;
          }
        }
      }
      contextValue.meta = connection.meta;
      contextValue.setMeta = (meta?: Generic) => this.meta(connection, meta);
      contextValue.bufferedAmount = () => connection.socket.bufferedAmount;
      const queries = document.definitions.filter((definition) => {
        return (definition as OperationDefinitionNode).operation === "query";
      });
      const mutations = document.definitions.filter((definition) => {
        return (definition as OperationDefinitionNode).operation === "mutation";
      });
      const subscriptions = document.definitions.filter((definition) => {
        return (
          (definition as OperationDefinitionNode).operation === "subscription"
        );
      });
      if ((queries.length || mutations.length) && subscriptions.length) {
        this.send(connection, {
          id: message.id,
          type: GRAPHQL.subscribe_error,
          errors: [
            "Cannot query and/or mutate and subscribe at the same time.",
          ],
        });
        this.send(connection, {
          id: message.id,
          type: GRAPHQL.subscribe_complete,
        });
        return;
      }
      const subscription: Subscription = {
        keepalive: new Timeout(() => {
          this.send(connection, {
            id: message.id,
            type: GRAPHQL.subscribe_ping,
          });
          subscription.keepalive.timeout = random(0, KEEPALIVE);
          subscription.keepalive.restart();
        }, random(0, KEEPALIVE)),
        timeout: new Timeout(() => {
          subscription.timeout.stop();
          subscription.keepalive.stop();
          subscription.cancel.signal({ reason: "timeout" });
          connection.subscriptions!.delete(message.id as string);
        }, TIMEOUT),
        cancel: new Signaler<{ reason: string }>(),
        complete: ({ reply }) => {
          subscription.timeout.stop();
          subscription.keepalive.stop();
          if (reply) {
            this.send(connection, {
              id: message.id,
              type: GRAPHQL.subscribe_complete,
            });
          }
          connection.subscriptions!.delete(message.id as string);
        },
      };
      connection.subscriptions!.set(message.id as string, subscription);
      let promises: Array<
        Promise<AsyncGenerator<ExecutionResult, void, void> | ExecutionResult>
      > = [];
      for (const definition of new Array<DefinitionNode>().concat(
        queries,
        mutations
      )) {
        const operationName = (definition as OperationDefinitionNode).name
          ?.value;
        let result: unknown;
        try {
          result = execute({
            rootValue: options.root,
            contextValue,
            variableValues,
            document,
            operationName,
            schema: options.schema,
          });
        } catch (error) {
          result = Promise.reject(error);
        }
        const promise = Promise.resolve(result);
        promises.push(promise as Promise<ExecutionResult>);
      }
      for (const promise of promises) {
        promise
          .then((result) => {
            if ((result as ExecutionResult).errors?.length) {
              this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_error,
                errors: (result as ExecutionResult).errors,
              });
            } else {
              this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_next,
                data: (result as ExecutionResult).data,
              });
            }
            promises = promises.filter((p) => p !== promise);
            if (promises.length === 0) {
              subscription.complete({ reply: true });
            }
          })
          .catch((error) => {
            this.send(connection, {
              id: message.id,
              type: GRAPHQL.subscribe_error,
              errors: [error.toString()],
            });
            promises = promises.filter((p) => p !== promise);
            if (promises.length === 0) {
              subscription.complete({ reply: true });
            }
          });
      }
      for (const definition of subscriptions) {
        const operationName = (definition as OperationDefinitionNode).name
          ?.value;
        let result: unknown;
        try {
          result = subscribe({
            rootValue: options.root,
            contextValue,
            variableValues,
            document,
            operationName,
            schema: options.schema,
          });
        } catch (error) {
          result = Promise.reject(error);
        }
        promises.push(
          result as Promise<
            AsyncGenerator<ExecutionResult, void, void> | ExecutionResult
          >
        );
        for (const promise of promises) {
          promise.then(async (result) => {
            if ((result as ExecutionResult).errors?.length) {
              this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_error,
                errors: (result as ExecutionResult).errors,
              });
              promises = promises.filter((p) => p !== promise);
              if (promises.length === 0) {
                subscription.complete({ reply: true });
              }
              return;
            }
            let race: unknown;
            const canceled = subscription.cancel.promise();
            const closed = connection.state!.next();
            do {
              race = await Promise.race([
                canceled,
                closed,
                (result as AsyncGenerator).next(),
              ]);
              if ((race as IteratorResult<unknown, unknown>).value) {
                const value = (race as IteratorResult<unknown, unknown>).value;
                const data = (value as ExecutionResult).data as Record<
                  string,
                  unknown
                >;
                if (data) {
                  const response: { data: Record<string, unknown> } = {
                    data: {},
                  };
                  for (const key of Object.keys(data)) {
                    if (data[key] !== NO_RESPONSE) {
                      response.data[key] = data[key];
                    }
                  }
                  if (Object.keys(response.data).length) {
                    this.send(connection, {
                      id: message.id,
                      type: GRAPHQL.subscribe_next,
                      data: response.data,
                    });
                  }
                }
              }
            } while (
              (race as IteratorResult<unknown, unknown>).value &&
              !(race as IteratorResult<unknown, unknown>).done
            );
            await (result as AsyncGenerator).return(null);
            promises = promises.filter((p) => p !== promise);
            if (promises.length === 0) {
              if (
                (race as { reason: string }).reason === "canceled" ||
                (race as IteratorResult<unknown, unknown>).done
              ) {
                subscription.complete({ reply: true });
              } else {
                subscription.complete({ reply: false });
              }
            }
          });
          /*
            .catch((error) => {
              this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_error,
                errors: [error.toString()],
              });
              promises = promises.filter((p) => p !== promise);
              if (promises.length === 0) {
                subscription.complete({ reply: true });
              }
            });
            */
        }
      }
    }
  );
}
