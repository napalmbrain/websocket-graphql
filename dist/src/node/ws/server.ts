import {
  NodeWebSocketServer,
  WebSocketServerOptions,
} from "@napalmbrain/websocket-manager";

import { initGraphQLServerHandlers } from "../../handler/server";
import {
  GraphQLServerOptions,
  GraphQLConnection,
  Subscription,
} from "../../common";

export class NodeGraphQLWebSocketServer extends NodeWebSocketServer {
  constructor(options: WebSocketServerOptions & GraphQLServerOptions) {
    super();
    initGraphQLServerHandlers.call(this, options);
  }

  public init(connection: GraphQLConnection) {
    if (!connection.subscriptions) {
      connection.subscriptions = new Map<string, Subscription>();
    }
    super.init(connection);
  }

  public deinit(connection: GraphQLConnection) {
    for (const subscription of connection.subscriptions!.values()) {
      subscription.keepalive.stop();
      subscription.timeout.stop();
    }
    connection.subscriptions!.clear();
    super.deinit(connection);
  }
}
