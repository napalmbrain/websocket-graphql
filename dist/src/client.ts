import { print } from "graphql";

import {
  Action,
  Connection,
  Message,
  Observer,
  WebSocketClient,
  WebSocketClientOptions,
  TIMEOUT,
} from "@napalmbrain/websocket-manager";

import { GRAPHQL, GraphQLOperation } from "./common";
import { initGraphQLClientHandlers } from "./handler/client";
import { isString } from "./utils";

export class GraphQLWebSocketClient extends WebSocketClient {
  constructor(options: WebSocketClientOptions = {}) {
    super(options);
    initGraphQLClientHandlers.call(this);
  }

  public cancel(connection: Connection, action: Action<Message>, wait = false) {
    if (!wait) {
      action.observable.complete();
    }
    this.send(connection, {
      id: action.message.id,
      type: GRAPHQL.subscribe_cancel,
    });
  }

  public subscribe(
    connection: Connection,
    operation: GraphQLOperation,
    observers: Observer<Message>[] = [],
    timeout = -1
  ): Action<Message> {
    let query = operation.query;
    if (!isString(query)) {
      // @ts-ignore: `query` should be printable.
      query = print(query);
    }
    return this.message(
      connection,
      {
        ...operation,
        query,
        type: GRAPHQL.subscribe_start,
      },
      observers,
      timeout
    );
  }
}
