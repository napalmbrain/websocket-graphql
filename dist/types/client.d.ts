import { Action, Connection, Message, Observer, WebSocketClient, WebSocketClientOptions } from "@napalmbrain/websocket-manager";
import { GraphQLOperation } from "./common";
export declare class GraphQLWebSocketClient extends WebSocketClient {
    constructor(options?: WebSocketClientOptions);
    cancel(connection: Connection, action: Action<Message>, wait?: boolean): void;
    subscribe(connection: Connection, operation: GraphQLOperation, observers?: Observer<Message>[], timeout?: number): Action<Message>;
}
//# sourceMappingURL=client.d.ts.map