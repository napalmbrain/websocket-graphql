import { BunWebSocketServer } from "@napalmbrain/websocket-manager/dist/src/bun/server";
import { WebSocketServerOptions } from "@napalmbrain/websocket-manager";
import { GraphQLServerOptions, GraphQLConnection } from "../common";
export declare class BunGraphQLWebSocketServer extends BunWebSocketServer {
    constructor(options: WebSocketServerOptions & GraphQLServerOptions);
    init(connection: GraphQLConnection): void;
    deinit(connection: GraphQLConnection): void;
}
//# sourceMappingURL=server.d.ts.map