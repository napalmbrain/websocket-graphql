import { WebSocketServer } from "@napalmbrain/websocket-manager";
import { GraphQLServerOptions } from "../common";
export declare function initGraphQLServerHandlers(this: WebSocketServer, options: GraphQLServerOptions): void;
//# sourceMappingURL=server.d.ts.map