import { ApolloLink, Operation, FetchResult, Observable, NextLink } from "@apollo/client/core";
import { WebSocketClientOptions } from "@napalmbrain/websocket-manager";
export declare class ApolloWebSocketLink extends ApolloLink {
    private url;
    private client;
    private connection;
    constructor(url: string | URL, options?: WebSocketClientOptions);
    request(operation: Operation, _forward?: NextLink | undefined): Observable<FetchResult> | null;
}
//# sourceMappingURL=link.d.ts.map