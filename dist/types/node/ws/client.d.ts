import { NodeWebSocketClient } from "@napalmbrain/websocket-manager";
import { Action, Connection, Message, Observer, WebSocketClientOptions } from "@napalmbrain/websocket-manager";
import { GraphQLOperation } from "../../common";
export declare class NodeGraphQLWebSocketClient extends NodeWebSocketClient {
    constructor(options?: WebSocketClientOptions);
    cancel(connection: Connection, action: Action<Message>, wait?: boolean): void;
    subscribe(connection: Connection, operation: GraphQLOperation, observers?: Observer<Message>[], retry?: number): Action<Message>;
}
//# sourceMappingURL=client.d.ts.map