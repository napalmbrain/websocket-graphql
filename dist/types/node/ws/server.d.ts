import { NodeWebSocketServer, WebSocketServerOptions } from "@napalmbrain/websocket-manager";
import { GraphQLServerOptions, GraphQLConnection } from "../../common";
export declare class NodeGraphQLWebSocketServer extends NodeWebSocketServer {
    constructor(options: WebSocketServerOptions & GraphQLServerOptions);
    init(connection: GraphQLConnection): void;
    deinit(connection: GraphQLConnection): void;
}
//# sourceMappingURL=server.d.ts.map