import { WebSocketServerOptions, NodeMicroWebSocketServer } from "@napalmbrain/websocket-manager";
import { GraphQLServerOptions, GraphQLConnection } from "../../common";
export declare class NodeMicroGraphQLWebSocketServer extends NodeMicroWebSocketServer {
    constructor(options: WebSocketServerOptions & GraphQLServerOptions);
    init(connection: GraphQLConnection): void;
    deinit(connection: GraphQLConnection): void;
}
//# sourceMappingURL=server.d.ts.map