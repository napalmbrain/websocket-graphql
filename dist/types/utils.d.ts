export declare function isString(value: unknown): boolean;
export declare function isFunction(value: unknown): boolean;
export declare function isAsyncGenerator(object: object): boolean;
//# sourceMappingURL=utils.d.ts.map