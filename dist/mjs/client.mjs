import { print } from "graphql";
import { WebSocketClient, } from "@napalmbrain/websocket-manager";
import { GRAPHQL } from "./common.mjs";
import { initGraphQLClientHandlers } from "./handler/client.mjs";
import { isString } from "./utils.mjs";
export class GraphQLWebSocketClient extends WebSocketClient {
    constructor(options = {}) {
        super(options);
        initGraphQLClientHandlers.call(this);
    }
    cancel(connection, action, wait = false) {
        if (!wait) {
            action.observable.complete();
        }
        this.send(connection, {
            id: action.message.id,
            type: GRAPHQL.subscribe_cancel,
        });
    }
    subscribe(connection, operation, observers = [], timeout = -1) {
        let query = operation.query;
        if (!isString(query)) {
            // @ts-ignore: `query` should be printable.
            query = print(query);
        }
        return this.message(connection, Object.assign(Object.assign({}, operation), { query, type: GRAPHQL.subscribe_start }), observers, timeout);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2NsaWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRWhDLE9BQU8sRUFLTCxlQUFlLEdBR2hCLE1BQU0sZ0NBQWdDLENBQUM7QUFFeEMsT0FBTyxFQUFFLE9BQU8sRUFBb0IsTUFBTSxVQUFVLENBQUM7QUFDckQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDN0QsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVuQyxNQUFNLE9BQU8sc0JBQXVCLFNBQVEsZUFBZTtJQUN6RCxZQUFZLFVBQWtDLEVBQUU7UUFDOUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2YseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFTSxNQUFNLENBQUMsVUFBc0IsRUFBRSxNQUF1QixFQUFFLElBQUksR0FBRyxLQUFLO1FBQ3pFLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDVCxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsRUFBRSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNyQixJQUFJLEVBQUUsT0FBTyxDQUFDLGdCQUFnQjtTQUMvQixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sU0FBUyxDQUNkLFVBQXNCLEVBQ3RCLFNBQTJCLEVBQzNCLFlBQWlDLEVBQUUsRUFDbkMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUVaLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7UUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNwQiwyQ0FBMkM7WUFDM0MsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN0QjtRQUNELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FDakIsVUFBVSxrQ0FFTCxTQUFTLEtBQ1osS0FBSyxFQUNMLElBQUksRUFBRSxPQUFPLENBQUMsZUFBZSxLQUUvQixTQUFTLEVBQ1QsT0FBTyxDQUNSLENBQUM7SUFDSixDQUFDO0NBQ0YifQ==