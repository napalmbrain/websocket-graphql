import { NodeMicroWebSocketServer, } from "@napalmbrain/websocket-manager";
import { initGraphQLServerHandlers } from "../../handler/server.mjs";
export class NodeMicroGraphQLWebSocketServer extends NodeMicroWebSocketServer {
    constructor(options) {
        super(options);
        initGraphQLServerHandlers.call(this, options);
    }
    // @ts-ignore ignore type error.
    init(connection) {
        if (!connection.subscriptions) {
            connection.subscriptions = new Map();
        }
        // @ts-ignore ignore type error.
        super.init(connection);
    }
    // @ts-ignore ignore type error.
    deinit(connection) {
        for (const subscription of connection.subscriptions.values()) {
            subscription.keepalive.stop();
            subscription.timeout.stop();
        }
        connection.subscriptions.clear();
        // @ts-ignore ignore type error.
        super.deinit(connection);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvdXdlYnNvY2tldHMvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFFTCx3QkFBd0IsR0FDekIsTUFBTSxnQ0FBZ0MsQ0FBQztBQUV4QyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQVFqRSxNQUFNLE9BQU8sK0JBQWdDLFNBQVEsd0JBQXdCO0lBQzNFLFlBQVksT0FBc0Q7UUFDaEUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2YseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsZ0NBQWdDO0lBQ3pCLElBQUksQ0FBQyxVQUE2QjtRQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRTtZQUM3QixVQUFVLENBQUMsYUFBYSxHQUFHLElBQUksR0FBRyxFQUF3QixDQUFDO1NBQzVEO1FBQ0QsZ0NBQWdDO1FBQ2hDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELGdDQUFnQztJQUN6QixNQUFNLENBQUMsVUFBNkI7UUFDekMsS0FBSyxNQUFNLFlBQVksSUFBSSxVQUFVLENBQUMsYUFBYyxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQzdELFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDOUIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUM3QjtRQUNELFVBQVUsQ0FBQyxhQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsZ0NBQWdDO1FBQ2hDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDM0IsQ0FBQztDQUNGIn0=