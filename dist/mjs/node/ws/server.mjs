import { NodeWebSocketServer, } from "@napalmbrain/websocket-manager";
import { initGraphQLServerHandlers } from "../../handler/server.mjs";
export class NodeGraphQLWebSocketServer extends NodeWebSocketServer {
    constructor(options) {
        super();
        initGraphQLServerHandlers.call(this, options);
    }
    init(connection) {
        if (!connection.subscriptions) {
            connection.subscriptions = new Map();
        }
        super.init(connection);
    }
    deinit(connection) {
        for (const subscription of connection.subscriptions.values()) {
            subscription.keepalive.stop();
            subscription.timeout.stop();
        }
        connection.subscriptions.clear();
        super.deinit(connection);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3Mvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxtQkFBbUIsR0FFcEIsTUFBTSxnQ0FBZ0MsQ0FBQztBQUV4QyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQU9qRSxNQUFNLE9BQU8sMEJBQTJCLFNBQVEsbUJBQW1CO0lBQ2pFLFlBQVksT0FBc0Q7UUFDaEUsS0FBSyxFQUFFLENBQUM7UUFDUix5QkFBeUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSxJQUFJLENBQUMsVUFBNkI7UUFDdkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUU7WUFDN0IsVUFBVSxDQUFDLGFBQWEsR0FBRyxJQUFJLEdBQUcsRUFBd0IsQ0FBQztTQUM1RDtRQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxVQUE2QjtRQUN6QyxLQUFLLE1BQU0sWUFBWSxJQUFJLFVBQVUsQ0FBQyxhQUFjLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDN0QsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzdCO1FBQ0QsVUFBVSxDQUFDLGFBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNCLENBQUM7Q0FDRiJ9