import { print } from "graphql";
import { NodeWebSocketClient } from "@napalmbrain/websocket-manager";
import { initGraphQLClientHandlers } from "../../handler/client.mjs";
import { GRAPHQL } from "../../common.mjs";
import { isString } from "../../utils.mjs";
export class NodeGraphQLWebSocketClient extends NodeWebSocketClient {
    constructor(options = {}) {
        super(options);
        initGraphQLClientHandlers.call(this);
    }
    cancel(connection, action, wait = false) {
        if (!wait) {
            action.observable.complete();
        }
        this.send(connection, {
            id: action.message.id,
            type: GRAPHQL.subscribe_cancel,
        });
    }
    subscribe(connection, operation, observers = [], retry = -1) {
        let query = operation.query;
        if (!isString(query)) {
            // @ts-ignore: `query` should be printable.
            query = print(query);
        }
        return this.message(connection, Object.assign(Object.assign({}, operation), { query, type: GRAPHQL.subscribe_start }), observers, retry);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3MvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDaEMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFXckUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDakUsT0FBTyxFQUFFLE9BQU8sRUFBb0IsTUFBTSxjQUFjLENBQUM7QUFDekQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUV2QyxNQUFNLE9BQU8sMEJBQTJCLFNBQVEsbUJBQW1CO0lBQ2pFLFlBQVksVUFBa0MsRUFBRTtRQUM5QyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDZix5QkFBeUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxVQUFzQixFQUFFLE1BQXVCLEVBQUUsSUFBSSxHQUFHLEtBQUs7UUFDekUsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNwQixFQUFFLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3JCLElBQUksRUFBRSxPQUFPLENBQUMsZ0JBQWdCO1NBQy9CLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxTQUFTLENBQ2QsVUFBc0IsRUFDdEIsU0FBMkIsRUFDM0IsWUFBaUMsRUFBRSxFQUNuQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBRVYsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLDJDQUEyQztZQUMzQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUNqQixVQUFVLGtDQUVMLFNBQVMsS0FDWixLQUFLLEVBQ0wsSUFBSSxFQUFFLE9BQU8sQ0FBQyxlQUFlLEtBRS9CLFNBQVMsRUFDVCxLQUFLLENBQ04sQ0FBQztJQUNKLENBQUM7Q0FDRiJ9