import { GRAPHQL } from "../common.mjs";
export function initGraphQLClientHandlers() {
    this.handle(GRAPHQL.subscribe_ping, (connection, message) => {
        var _a;
        const action = connection.actions.get(message.id);
        if (action) {
            (_a = action.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_pong,
            });
        }
    });
    this.handle(GRAPHQL.subscribe_next, (connection, message) => {
        var _a;
        (_a = connection.actions.get(message.id)) === null || _a === void 0 ? void 0 : _a.observable.next(message);
    });
    this.handle(GRAPHQL.subscribe_error, (connection, message) => {
        var _a;
        (_a = connection.actions.get(message.id)) === null || _a === void 0 ? void 0 : _a.observable.error(message);
    });
    this.handle(GRAPHQL.subscribe_complete, (connection, message) => {
        var _a;
        (_a = connection.actions.get(message.id)) === null || _a === void 0 ? void 0 : _a.observable.complete();
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2hhbmRsZXIvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFFcEMsTUFBTSxVQUFVLHlCQUF5QjtJQUN2QyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQzFELE1BQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxPQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsQ0FBQztRQUM3RCxJQUFJLE1BQU0sRUFBRTtZQUNWLE1BQUEsTUFBTSxDQUFDLE9BQU8sMENBQUUsT0FBTyxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGNBQWM7YUFDN0IsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDLENBQUMsQ0FBQztJQUNILElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsRUFBRTs7UUFDMUQsTUFBQSxVQUFVLENBQUMsT0FBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLDBDQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDMUUsQ0FBQyxDQUFDLENBQUM7SUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQzNELE1BQUEsVUFBVSxDQUFDLE9BQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzNFLENBQUMsQ0FBQyxDQUFDO0lBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQzlELE1BQUEsVUFBVSxDQUFDLE9BQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkUsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDIn0=