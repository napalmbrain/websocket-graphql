var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { execute, parse, subscribe, validate, } from "graphql";
import { Signaler, Timeout, TIMEOUT, KEEPALIVE, random, } from "@napalmbrain/websocket-manager";
import { GRAPHQL, NO_RESPONSE, } from "../common.mjs";
import { isFunction } from "../utils.mjs";
export function initGraphQLServerHandlers(options) {
    this.handle(GRAPHQL.subscribe_pong, (connection, message) => {
        var _a;
        (_a = connection.subscriptions.get(message.id)) === null || _a === void 0 ? void 0 : _a.timeout.restart();
    });
    this.handle(GRAPHQL.subscribe_cancel, (connection, message) => {
        var _a;
        (_a = connection
            .subscriptions.get(message.id)) === null || _a === void 0 ? void 0 : _a.cancel.signal({ reason: "canceled" });
    });
    this.handle(GRAPHQL.subscribe_start, (connection, message) => __awaiter(this, void 0, void 0, function* () {
        var _a, _b;
        let document;
        try {
            document = parse(message.query);
        }
        catch (error) {
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_error,
                errors: [error.toString()],
            });
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_complete,
            });
            return;
        }
        const validationErrors = validate(options.schema, document);
        if (validationErrors.length) {
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_error,
                errors: validationErrors,
            });
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_complete,
            });
            return;
        }
        let contextValue = {};
        const variableValues = message.variables;
        if (options.context) {
            if (isFunction(options.context)) {
                //@ts-ignore: `this.context` is callable here.
                const context = yield options.context(context);
                if (context) {
                    contextValue = context;
                }
            }
        }
        contextValue.meta = connection.meta;
        contextValue.setMeta = (meta) => this.meta(connection, meta);
        contextValue.bufferedAmount = () => connection.socket.bufferedAmount;
        const queries = document.definitions.filter((definition) => {
            return definition.operation === "query";
        });
        const mutations = document.definitions.filter((definition) => {
            return definition.operation === "mutation";
        });
        const subscriptions = document.definitions.filter((definition) => {
            return (definition.operation === "subscription");
        });
        if ((queries.length || mutations.length) && subscriptions.length) {
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_error,
                errors: [
                    "Cannot query and/or mutate and subscribe at the same time.",
                ],
            });
            this.send(connection, {
                id: message.id,
                type: GRAPHQL.subscribe_complete,
            });
            return;
        }
        const subscription = {
            keepalive: new Timeout(() => {
                this.send(connection, {
                    id: message.id,
                    type: GRAPHQL.subscribe_ping,
                });
                subscription.keepalive.timeout = random(0, KEEPALIVE);
                subscription.keepalive.restart();
            }, random(0, KEEPALIVE)),
            timeout: new Timeout(() => {
                subscription.timeout.stop();
                subscription.keepalive.stop();
                subscription.cancel.signal({ reason: "timeout" });
                connection.subscriptions.delete(message.id);
            }, TIMEOUT),
            cancel: new Signaler(),
            complete: ({ reply }) => {
                subscription.timeout.stop();
                subscription.keepalive.stop();
                if (reply) {
                    this.send(connection, {
                        id: message.id,
                        type: GRAPHQL.subscribe_complete,
                    });
                }
                connection.subscriptions.delete(message.id);
            },
        };
        connection.subscriptions.set(message.id, subscription);
        let promises = [];
        for (const definition of new Array().concat(queries, mutations)) {
            const operationName = (_a = definition.name) === null || _a === void 0 ? void 0 : _a.value;
            let result;
            try {
                result = execute({
                    rootValue: options.root,
                    contextValue,
                    variableValues,
                    document,
                    operationName,
                    schema: options.schema,
                });
            }
            catch (error) {
                result = Promise.reject(error);
            }
            const promise = Promise.resolve(result);
            promises.push(promise);
        }
        for (const promise of promises) {
            promise
                .then((result) => {
                var _a;
                if ((_a = result.errors) === null || _a === void 0 ? void 0 : _a.length) {
                    this.send(connection, {
                        id: message.id,
                        type: GRAPHQL.subscribe_error,
                        errors: result.errors,
                    });
                }
                else {
                    this.send(connection, {
                        id: message.id,
                        type: GRAPHQL.subscribe_next,
                        data: result.data,
                    });
                }
                promises = promises.filter((p) => p !== promise);
                if (promises.length === 0) {
                    subscription.complete({ reply: true });
                }
            })
                .catch((error) => {
                this.send(connection, {
                    id: message.id,
                    type: GRAPHQL.subscribe_error,
                    errors: [error.toString()],
                });
                promises = promises.filter((p) => p !== promise);
                if (promises.length === 0) {
                    subscription.complete({ reply: true });
                }
            });
        }
        for (const definition of subscriptions) {
            const operationName = (_b = definition.name) === null || _b === void 0 ? void 0 : _b.value;
            let result;
            try {
                result = subscribe({
                    rootValue: options.root,
                    contextValue,
                    variableValues,
                    document,
                    operationName,
                    schema: options.schema,
                });
            }
            catch (error) {
                result = Promise.reject(error);
            }
            promises.push(result);
            for (const promise of promises) {
                promise.then((result) => __awaiter(this, void 0, void 0, function* () {
                    var _c;
                    if ((_c = result.errors) === null || _c === void 0 ? void 0 : _c.length) {
                        this.send(connection, {
                            id: message.id,
                            type: GRAPHQL.subscribe_error,
                            errors: result.errors,
                        });
                        promises = promises.filter((p) => p !== promise);
                        if (promises.length === 0) {
                            subscription.complete({ reply: true });
                        }
                        return;
                    }
                    let race;
                    const canceled = subscription.cancel.promise();
                    const closed = connection.state.next();
                    do {
                        race = yield Promise.race([
                            canceled,
                            closed,
                            result.next(),
                        ]);
                        if (race.value) {
                            const value = race.value;
                            const data = value.data;
                            if (data) {
                                const response = {
                                    data: {},
                                };
                                for (const key of Object.keys(data)) {
                                    if (data[key] !== NO_RESPONSE) {
                                        response.data[key] = data[key];
                                    }
                                }
                                if (Object.keys(response.data).length) {
                                    this.send(connection, {
                                        id: message.id,
                                        type: GRAPHQL.subscribe_next,
                                        data: response.data,
                                    });
                                }
                            }
                        }
                    } while (race.value &&
                        !race.done);
                    yield result.return(null);
                    promises = promises.filter((p) => p !== promise);
                    if (promises.length === 0) {
                        if (race.reason === "canceled" ||
                            race.done) {
                            subscription.complete({ reply: true });
                        }
                        else {
                            subscription.complete({ reply: false });
                        }
                    }
                }));
                /*
                  .catch((error) => {
                    this.send(connection, {
                      id: message.id,
                      type: GRAPHQL.subscribe_error,
                      errors: [error.toString()],
                    });
                    promises = promises.filter((p) => p !== promise);
                    if (promises.length === 0) {
                      subscription.complete({ reply: true });
                    }
                  });
                  */
            }
        }
    }));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2hhbmRsZXIvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE9BQU8sRUFFTCxPQUFPLEVBQ1AsS0FBSyxFQUNMLFNBQVMsRUFDVCxRQUFRLEdBS1QsTUFBTSxTQUFTLENBQUM7QUFFakIsT0FBTyxFQUVMLFFBQVEsRUFDUixPQUFPLEVBQ1AsT0FBTyxFQUNQLFNBQVMsRUFFVCxNQUFNLEdBQ1AsTUFBTSxnQ0FBZ0MsQ0FBQztBQUV4QyxPQUFPLEVBQ0wsT0FBTyxFQUlQLFdBQVcsR0FDWixNQUFNLFdBQVcsQ0FBQztBQUNuQixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRXRDLE1BQU0sVUFBVSx5QkFBeUIsQ0FFdkMsT0FBNkI7SUFFN0IsSUFBSSxDQUFDLE1BQU0sQ0FDVCxPQUFPLENBQUMsY0FBYyxFQUN0QixDQUFDLFVBQTZCLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQ3pDLE1BQUEsVUFBVSxDQUFDLGFBQWMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDekUsQ0FBQyxDQUNGLENBQUM7SUFDRixJQUFJLENBQUMsTUFBTSxDQUNULE9BQU8sQ0FBQyxnQkFBZ0IsRUFDeEIsQ0FBQyxVQUE2QixFQUFFLE9BQU8sRUFBRSxFQUFFOztRQUN6QyxNQUFBLFVBQVU7YUFDUCxhQUFjLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsMENBQ3ZDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztJQUM1QyxDQUFDLENBQ0YsQ0FBQztJQUNGLElBQUksQ0FBQyxNQUFNLENBQ1QsT0FBTyxDQUFDLGVBQWUsRUFDdkIsQ0FBTyxVQUE2QixFQUFFLE9BQU8sRUFBRSxFQUFFOztRQUMvQyxJQUFJLFFBQXNCLENBQUM7UUFDM0IsSUFBSTtZQUNGLFFBQVEsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQWUsQ0FBQyxDQUFDO1NBQzNDO1FBQUMsT0FBTyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxPQUFPLENBQUMsZUFBZTtnQkFDN0IsTUFBTSxFQUFFLENBQUUsS0FBc0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUM3QyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxPQUFPLENBQUMsa0JBQWtCO2FBQ2pDLENBQUMsQ0FBQztZQUNILE9BQU87U0FDUjtRQUNELE1BQU0sZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUQsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGVBQWU7Z0JBQzdCLE1BQU0sRUFBRSxnQkFBZ0I7YUFDekIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGtCQUFrQjthQUNqQyxDQUFDLENBQUM7WUFDSCxPQUFPO1NBQ1I7UUFDRCxJQUFJLFlBQVksR0FBNEIsRUFBRSxDQUFDO1FBQy9DLE1BQU0sY0FBYyxHQUFHLE9BQU8sQ0FBQyxTQUFvQyxDQUFDO1FBQ3BFLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQy9CLDhDQUE4QztnQkFDOUMsTUFBTSxPQUFPLEdBQUcsTUFBTSxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLE9BQU8sRUFBRTtvQkFDWCxZQUFZLEdBQUcsT0FBTyxDQUFDO2lCQUN4QjthQUNGO1NBQ0Y7UUFDRCxZQUFZLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUM7UUFDcEMsWUFBWSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQWMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkUsWUFBWSxDQUFDLGNBQWMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQztRQUNyRSxNQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQ3pELE9BQVEsVUFBc0MsQ0FBQyxTQUFTLEtBQUssT0FBTyxDQUFDO1FBQ3ZFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUMzRCxPQUFRLFVBQXNDLENBQUMsU0FBUyxLQUFLLFVBQVUsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQztRQUNILE1BQU0sYUFBYSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDL0QsT0FBTyxDQUNKLFVBQXNDLENBQUMsU0FBUyxLQUFLLGNBQWMsQ0FDckUsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDaEUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGVBQWU7Z0JBQzdCLE1BQU0sRUFBRTtvQkFDTiw0REFBNEQ7aUJBQzdEO2FBQ0YsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGtCQUFrQjthQUNqQyxDQUFDLENBQUM7WUFDSCxPQUFPO1NBQ1I7UUFDRCxNQUFNLFlBQVksR0FBaUI7WUFDakMsU0FBUyxFQUFFLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtvQkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGNBQWM7aUJBQzdCLENBQUMsQ0FBQztnQkFDSCxZQUFZLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUN0RCxZQUFZLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ25DLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sRUFBRSxJQUFJLE9BQU8sQ0FBQyxHQUFHLEVBQUU7Z0JBQ3hCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzVCLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xELFVBQVUsQ0FBQyxhQUFjLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsQ0FBQztZQUN6RCxDQUFDLEVBQUUsT0FBTyxDQUFDO1lBQ1gsTUFBTSxFQUFFLElBQUksUUFBUSxFQUFzQjtZQUMxQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUU7Z0JBQ3RCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzVCLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLElBQUksS0FBSyxFQUFFO29CQUNULElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO3dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxFQUFFLE9BQU8sQ0FBQyxrQkFBa0I7cUJBQ2pDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxVQUFVLENBQUMsYUFBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLENBQUM7WUFDekQsQ0FBQztTQUNGLENBQUM7UUFDRixVQUFVLENBQUMsYUFBYyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQ2xFLElBQUksUUFBUSxHQUVSLEVBQUUsQ0FBQztRQUNQLEtBQUssTUFBTSxVQUFVLElBQUksSUFBSSxLQUFLLEVBQWtCLENBQUMsTUFBTSxDQUN6RCxPQUFPLEVBQ1AsU0FBUyxDQUNWLEVBQUU7WUFDRCxNQUFNLGFBQWEsR0FBRyxNQUFDLFVBQXNDLENBQUMsSUFBSSwwQ0FDOUQsS0FBSyxDQUFDO1lBQ1YsSUFBSSxNQUFlLENBQUM7WUFDcEIsSUFBSTtnQkFDRixNQUFNLEdBQUcsT0FBTyxDQUFDO29CQUNmLFNBQVMsRUFBRSxPQUFPLENBQUMsSUFBSTtvQkFDdkIsWUFBWTtvQkFDWixjQUFjO29CQUNkLFFBQVE7b0JBQ1IsYUFBYTtvQkFDYixNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU07aUJBQ3ZCLENBQUMsQ0FBQzthQUNKO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDaEM7WUFDRCxNQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3hDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBbUMsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsS0FBSyxNQUFNLE9BQU8sSUFBSSxRQUFRLEVBQUU7WUFDOUIsT0FBTztpQkFDSixJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2YsSUFBSSxNQUFDLE1BQTBCLENBQUMsTUFBTSwwQ0FBRSxNQUFNLEVBQUU7b0JBQzlDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO3dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxFQUFFLE9BQU8sQ0FBQyxlQUFlO3dCQUM3QixNQUFNLEVBQUcsTUFBMEIsQ0FBQyxNQUFNO3FCQUMzQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7d0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTt3QkFDZCxJQUFJLEVBQUUsT0FBTyxDQUFDLGNBQWM7d0JBQzVCLElBQUksRUFBRyxNQUEwQixDQUFDLElBQUk7cUJBQ3ZDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN6QixZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0gsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxFQUFFLE9BQU8sQ0FBQyxlQUFlO29CQUM3QixNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQzNCLENBQUMsQ0FBQztnQkFDSCxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN6QixZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELEtBQUssTUFBTSxVQUFVLElBQUksYUFBYSxFQUFFO1lBQ3RDLE1BQU0sYUFBYSxHQUFHLE1BQUMsVUFBc0MsQ0FBQyxJQUFJLDBDQUM5RCxLQUFLLENBQUM7WUFDVixJQUFJLE1BQWUsQ0FBQztZQUNwQixJQUFJO2dCQUNGLE1BQU0sR0FBRyxTQUFTLENBQUM7b0JBQ2pCLFNBQVMsRUFBRSxPQUFPLENBQUMsSUFBSTtvQkFDdkIsWUFBWTtvQkFDWixjQUFjO29CQUNkLFFBQVE7b0JBQ1IsYUFBYTtvQkFDYixNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU07aUJBQ3ZCLENBQUMsQ0FBQzthQUNKO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDaEM7WUFDRCxRQUFRLENBQUMsSUFBSSxDQUNYLE1BRUMsQ0FDRixDQUFDO1lBQ0YsS0FBSyxNQUFNLE9BQU8sSUFBSSxRQUFRLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBTyxNQUFNLEVBQUUsRUFBRTs7b0JBQzVCLElBQUksTUFBQyxNQUEwQixDQUFDLE1BQU0sMENBQUUsTUFBTSxFQUFFO3dCQUM5QyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTs0QkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFOzRCQUNkLElBQUksRUFBRSxPQUFPLENBQUMsZUFBZTs0QkFDN0IsTUFBTSxFQUFHLE1BQTBCLENBQUMsTUFBTTt5QkFDM0MsQ0FBQyxDQUFDO3dCQUNILFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssT0FBTyxDQUFDLENBQUM7d0JBQ2pELElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7NEJBQ3pCLFlBQVksQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt5QkFDeEM7d0JBQ0QsT0FBTztxQkFDUjtvQkFDRCxJQUFJLElBQWEsQ0FBQztvQkFDbEIsTUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDL0MsTUFBTSxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDeEMsR0FBRzt3QkFDRCxJQUFJLEdBQUcsTUFBTSxPQUFPLENBQUMsSUFBSSxDQUFDOzRCQUN4QixRQUFROzRCQUNSLE1BQU07NEJBQ0wsTUFBeUIsQ0FBQyxJQUFJLEVBQUU7eUJBQ2xDLENBQUMsQ0FBQzt3QkFDSCxJQUFLLElBQXlDLENBQUMsS0FBSyxFQUFFOzRCQUNwRCxNQUFNLEtBQUssR0FBSSxJQUF5QyxDQUFDLEtBQUssQ0FBQzs0QkFDL0QsTUFBTSxJQUFJLEdBQUksS0FBeUIsQ0FBQyxJQUd2QyxDQUFDOzRCQUNGLElBQUksSUFBSSxFQUFFO2dDQUNSLE1BQU0sUUFBUSxHQUFzQztvQ0FDbEQsSUFBSSxFQUFFLEVBQUU7aUNBQ1QsQ0FBQztnQ0FDRixLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7b0NBQ25DLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLFdBQVcsRUFBRTt3Q0FDN0IsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7cUNBQ2hDO2lDQUNGO2dDQUNELElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFO29DQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTt3Q0FDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO3dDQUNkLElBQUksRUFBRSxPQUFPLENBQUMsY0FBYzt3Q0FDNUIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxJQUFJO3FDQUNwQixDQUFDLENBQUM7aUNBQ0o7NkJBQ0Y7eUJBQ0Y7cUJBQ0YsUUFDRSxJQUF5QyxDQUFDLEtBQUs7d0JBQ2hELENBQUUsSUFBeUMsQ0FBQyxJQUFJLEVBQ2hEO29CQUNGLE1BQU8sTUFBeUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzlDLFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssT0FBTyxDQUFDLENBQUM7b0JBQ2pELElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7d0JBQ3pCLElBQ0csSUFBMkIsQ0FBQyxNQUFNLEtBQUssVUFBVTs0QkFDakQsSUFBeUMsQ0FBQyxJQUFJLEVBQy9DOzRCQUNBLFlBQVksQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt5QkFDeEM7NkJBQU07NEJBQ0wsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO3lCQUN6QztxQkFDRjtnQkFDSCxDQUFDLENBQUEsQ0FBQyxDQUFDO2dCQUNIOzs7Ozs7Ozs7Ozs7b0JBWUk7YUFDTDtTQUNGO0lBQ0gsQ0FBQyxDQUFBLENBQ0YsQ0FBQztBQUNKLENBQUMifQ==