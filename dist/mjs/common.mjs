export var GRAPHQL;
(function (GRAPHQL) {
    GRAPHQL["subscribe_start"] = "subscribe_start";
    GRAPHQL["subscribe_next"] = "subscribe_next";
    GRAPHQL["subscribe_error"] = "subscribe_error";
    GRAPHQL["subscribe_cancel"] = "subscribe_cancel";
    GRAPHQL["subscribe_complete"] = "subscribe_complete";
    GRAPHQL["subscribe_ping"] = "subscribe_ping";
    GRAPHQL["subscribe_pong"] = "subscribe_pong";
})(GRAPHQL || (GRAPHQL = {}));
export const NO_RESPONSE = "#no-response";
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2NvbW1vbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFZQSxNQUFNLENBQU4sSUFBWSxPQVFYO0FBUkQsV0FBWSxPQUFPO0lBQ2pCLDhDQUFtQyxDQUFBO0lBQ25DLDRDQUFpQyxDQUFBO0lBQ2pDLDhDQUFtQyxDQUFBO0lBQ25DLGdEQUFxQyxDQUFBO0lBQ3JDLG9EQUF5QyxDQUFBO0lBQ3pDLDRDQUFpQyxDQUFBO0lBQ2pDLDRDQUFpQyxDQUFBO0FBQ25DLENBQUMsRUFSVyxPQUFPLEtBQVAsT0FBTyxRQVFsQjtBQUVELE1BQU0sQ0FBQyxNQUFNLFdBQVcsR0FBRyxjQUFjLENBQUMifQ==