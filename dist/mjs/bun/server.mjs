import { BunWebSocketServer } from "@napalmbrain/websocket-manager/dist/src/bun/server";
import { initGraphQLServerHandlers } from "../handler/server.mjs";
export class BunGraphQLWebSocketServer extends BunWebSocketServer {
    constructor(options) {
        // @ts-ignore ignore type error.
        super(options);
        // @ts-ignore ignore type error.
        initGraphQLServerHandlers.call(this, options);
    }
    // @ts-ignore ignore type error.
    init(connection) {
        if (!connection.subscriptions) {
            connection.subscriptions = new Map();
        }
        // @ts-ignore ignore type error.
        super.init(connection);
    }
    // @ts-ignore ignore type error.
    deinit(connection) {
        for (const subscription of connection.subscriptions.values()) {
            subscription.keepalive.stop();
            subscription.timeout.stop();
        }
        connection.subscriptions.clear();
        // @ts-ignore ignore type error.
        super.deinit(connection);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2J1bi9zZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFHeEYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFPOUQsTUFBTSxPQUFPLHlCQUEwQixTQUFRLGtCQUFrQjtJQUMvRCxZQUFZLE9BQXNEO1FBQ2hFLGdDQUFnQztRQUNoQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDZixnQ0FBZ0M7UUFDaEMseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsZ0NBQWdDO0lBQ3pCLElBQUksQ0FBQyxVQUE2QjtRQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRTtZQUM3QixVQUFVLENBQUMsYUFBYSxHQUFHLElBQUksR0FBRyxFQUF3QixDQUFDO1NBQzVEO1FBQ0QsZ0NBQWdDO1FBQ2hDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELGdDQUFnQztJQUN6QixNQUFNLENBQUMsVUFBNkI7UUFDekMsS0FBSyxNQUFNLFlBQVksSUFBSSxVQUFVLENBQUMsYUFBYyxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQzdELFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDOUIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUM3QjtRQUNELFVBQVUsQ0FBQyxhQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsZ0NBQWdDO1FBQ2hDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDM0IsQ0FBQztDQUNGIn0=