import { ApolloLink, Observable, } from "@apollo/client/core";
import { GraphQLWebSocketClient } from "./client.mjs";
export class ApolloWebSocketLink extends ApolloLink {
    constructor(url, options) {
        super();
        this.url = url;
        this.client = new GraphQLWebSocketClient(options);
        this.connection = this.client.connect(this.url);
    }
    request(operation, _forward) {
        return new Observable((observer) => {
            const action = this.client.subscribe(this.connection, 
            // deno-lint-ignore no-explicit-any
            operation, [observer], -1);
            return () => this.client.cancel(this.connection, action);
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGluay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9saW5rLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxVQUFVLEVBR1YsVUFBVSxHQUVYLE1BQU0scUJBQXFCLENBQUM7QUFPN0IsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRWxELE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxVQUFVO0lBS2pELFlBQVksR0FBaUIsRUFBRSxPQUFnQztRQUM3RCxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTSxPQUFPLENBQ1osU0FBb0IsRUFDcEIsUUFBK0I7UUFFL0IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ2pDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUNsQyxJQUFJLENBQUMsVUFBVTtZQUNmLG1DQUFtQztZQUNuQyxTQUFnQixFQUNoQixDQUFDLFFBQVEsQ0FBQyxFQUNWLENBQUMsQ0FBQyxDQUNILENBQUM7WUFDRixPQUFPLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDM0QsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0YifQ==