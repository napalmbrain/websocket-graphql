"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GraphQLWebSocketClient = void 0;
const graphql_1 = require("graphql");
const websocket_manager_1 = require("@napalmbrain/websocket-manager");
const common_1 = require("./common");
const client_1 = require("./handler/client");
const utils_1 = require("./utils");
class GraphQLWebSocketClient extends websocket_manager_1.WebSocketClient {
    constructor(options = {}) {
        super(options);
        client_1.initGraphQLClientHandlers.call(this);
    }
    cancel(connection, action, wait = false) {
        if (!wait) {
            action.observable.complete();
        }
        this.send(connection, {
            id: action.message.id,
            type: common_1.GRAPHQL.subscribe_cancel,
        });
    }
    subscribe(connection, operation, observers = [], timeout = -1) {
        let query = operation.query;
        if (!(0, utils_1.isString)(query)) {
            // @ts-ignore: `query` should be printable.
            query = (0, graphql_1.print)(query);
        }
        return this.message(connection, Object.assign(Object.assign({}, operation), { query, type: common_1.GRAPHQL.subscribe_start }), observers, timeout);
    }
}
exports.GraphQLWebSocketClient = GraphQLWebSocketClient;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2NsaWVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxxQ0FBZ0M7QUFFaEMsc0VBUXdDO0FBRXhDLHFDQUFxRDtBQUNyRCw2Q0FBNkQ7QUFDN0QsbUNBQW1DO0FBRW5DLE1BQWEsc0JBQXVCLFNBQVEsbUNBQWU7SUFDekQsWUFBWSxVQUFrQyxFQUFFO1FBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNmLGtDQUF5QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCLEVBQUUsTUFBdUIsRUFBRSxJQUFJLEdBQUcsS0FBSztRQUN6RSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3BCLEVBQUUsRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDckIsSUFBSSxFQUFFLGdCQUFPLENBQUMsZ0JBQWdCO1NBQy9CLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxTQUFTLENBQ2QsVUFBc0IsRUFDdEIsU0FBMkIsRUFDM0IsWUFBaUMsRUFBRSxFQUNuQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBRVosSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsSUFBQSxnQkFBUSxFQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLDJDQUEyQztZQUMzQyxLQUFLLEdBQUcsSUFBQSxlQUFLLEVBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEI7UUFDRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQ2pCLFVBQVUsa0NBRUwsU0FBUyxLQUNaLEtBQUssRUFDTCxJQUFJLEVBQUUsZ0JBQU8sQ0FBQyxlQUFlLEtBRS9CLFNBQVMsRUFDVCxPQUFPLENBQ1IsQ0FBQztJQUNKLENBQUM7Q0FDRjtBQXRDRCx3REFzQ0MifQ==