"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initGraphQLClientHandlers = void 0;
const common_1 = require("../common");
function initGraphQLClientHandlers() {
    this.handle(common_1.GRAPHQL.subscribe_ping, (connection, message) => {
        var _a;
        const action = connection.actions.get(message.id);
        if (action) {
            (_a = action.timeout) === null || _a === void 0 ? void 0 : _a.restart();
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_pong,
            });
        }
    });
    this.handle(common_1.GRAPHQL.subscribe_next, (connection, message) => {
        var _a;
        (_a = connection.actions.get(message.id)) === null || _a === void 0 ? void 0 : _a.observable.next(message);
    });
    this.handle(common_1.GRAPHQL.subscribe_error, (connection, message) => {
        var _a;
        (_a = connection.actions.get(message.id)) === null || _a === void 0 ? void 0 : _a.observable.error(message);
    });
    this.handle(common_1.GRAPHQL.subscribe_complete, (connection, message) => {
        var _a;
        (_a = connection.actions.get(message.id)) === null || _a === void 0 ? void 0 : _a.observable.complete();
    });
}
exports.initGraphQLClientHandlers = initGraphQLClientHandlers;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2hhbmRsZXIvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUVBLHNDQUFvQztBQUVwQyxTQUFnQix5QkFBeUI7SUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsRUFBRTs7UUFDMUQsTUFBTSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQyxDQUFDO1FBQzdELElBQUksTUFBTSxFQUFFO1lBQ1YsTUFBQSxNQUFNLENBQUMsT0FBTywwQ0FBRSxPQUFPLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxnQkFBTyxDQUFDLGNBQWM7YUFDN0IsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDLENBQUMsQ0FBQztJQUNILElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQzFELE1BQUEsVUFBVSxDQUFDLE9BQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzFFLENBQUMsQ0FBQyxDQUFDO0lBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBTyxDQUFDLGVBQWUsRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsRUFBRTs7UUFDM0QsTUFBQSxVQUFVLENBQUMsT0FBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLDBDQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0UsQ0FBQyxDQUFDLENBQUM7SUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQzlELE1BQUEsVUFBVSxDQUFDLE9BQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FBRSxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkUsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDO0FBcEJELDhEQW9CQyJ9