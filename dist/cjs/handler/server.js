"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initGraphQLServerHandlers = void 0;
const graphql_1 = require("graphql");
const websocket_manager_1 = require("@napalmbrain/websocket-manager");
const common_1 = require("../common");
const utils_1 = require("../utils");
function initGraphQLServerHandlers(options) {
    this.handle(common_1.GRAPHQL.subscribe_pong, (connection, message) => {
        var _a;
        (_a = connection.subscriptions.get(message.id)) === null || _a === void 0 ? void 0 : _a.timeout.restart();
    });
    this.handle(common_1.GRAPHQL.subscribe_cancel, (connection, message) => {
        var _a;
        (_a = connection
            .subscriptions.get(message.id)) === null || _a === void 0 ? void 0 : _a.cancel.signal({ reason: "canceled" });
    });
    this.handle(common_1.GRAPHQL.subscribe_start, (connection, message) => __awaiter(this, void 0, void 0, function* () {
        var _a, _b;
        let document;
        try {
            document = (0, graphql_1.parse)(message.query);
        }
        catch (error) {
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_error,
                errors: [error.toString()],
            });
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_complete,
            });
            return;
        }
        const validationErrors = (0, graphql_1.validate)(options.schema, document);
        if (validationErrors.length) {
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_error,
                errors: validationErrors,
            });
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_complete,
            });
            return;
        }
        let contextValue = {};
        const variableValues = message.variables;
        if (options.context) {
            if ((0, utils_1.isFunction)(options.context)) {
                //@ts-ignore: `this.context` is callable here.
                const context = yield options.context(context);
                if (context) {
                    contextValue = context;
                }
            }
        }
        contextValue.meta = connection.meta;
        contextValue.setMeta = (meta) => this.meta(connection, meta);
        contextValue.bufferedAmount = () => connection.socket.bufferedAmount;
        const queries = document.definitions.filter((definition) => {
            return definition.operation === "query";
        });
        const mutations = document.definitions.filter((definition) => {
            return definition.operation === "mutation";
        });
        const subscriptions = document.definitions.filter((definition) => {
            return (definition.operation === "subscription");
        });
        if ((queries.length || mutations.length) && subscriptions.length) {
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_error,
                errors: [
                    "Cannot query and/or mutate and subscribe at the same time.",
                ],
            });
            this.send(connection, {
                id: message.id,
                type: common_1.GRAPHQL.subscribe_complete,
            });
            return;
        }
        const subscription = {
            keepalive: new websocket_manager_1.Timeout(() => {
                this.send(connection, {
                    id: message.id,
                    type: common_1.GRAPHQL.subscribe_ping,
                });
                subscription.keepalive.timeout = (0, websocket_manager_1.random)(0, websocket_manager_1.KEEPALIVE);
                subscription.keepalive.restart();
            }, (0, websocket_manager_1.random)(0, websocket_manager_1.KEEPALIVE)),
            timeout: new websocket_manager_1.Timeout(() => {
                subscription.timeout.stop();
                subscription.keepalive.stop();
                subscription.cancel.signal({ reason: "timeout" });
                connection.subscriptions.delete(message.id);
            }, websocket_manager_1.TIMEOUT),
            cancel: new websocket_manager_1.Signaler(),
            complete: ({ reply }) => {
                subscription.timeout.stop();
                subscription.keepalive.stop();
                if (reply) {
                    this.send(connection, {
                        id: message.id,
                        type: common_1.GRAPHQL.subscribe_complete,
                    });
                }
                connection.subscriptions.delete(message.id);
            },
        };
        connection.subscriptions.set(message.id, subscription);
        let promises = [];
        for (const definition of new Array().concat(queries, mutations)) {
            const operationName = (_a = definition.name) === null || _a === void 0 ? void 0 : _a.value;
            let result;
            try {
                result = (0, graphql_1.execute)({
                    rootValue: options.root,
                    contextValue,
                    variableValues,
                    document,
                    operationName,
                    schema: options.schema,
                });
            }
            catch (error) {
                result = Promise.reject(error);
            }
            const promise = Promise.resolve(result);
            promises.push(promise);
        }
        for (const promise of promises) {
            promise
                .then((result) => {
                var _a;
                if ((_a = result.errors) === null || _a === void 0 ? void 0 : _a.length) {
                    this.send(connection, {
                        id: message.id,
                        type: common_1.GRAPHQL.subscribe_error,
                        errors: result.errors,
                    });
                }
                else {
                    this.send(connection, {
                        id: message.id,
                        type: common_1.GRAPHQL.subscribe_next,
                        data: result.data,
                    });
                }
                promises = promises.filter((p) => p !== promise);
                if (promises.length === 0) {
                    subscription.complete({ reply: true });
                }
            })
                .catch((error) => {
                this.send(connection, {
                    id: message.id,
                    type: common_1.GRAPHQL.subscribe_error,
                    errors: [error.toString()],
                });
                promises = promises.filter((p) => p !== promise);
                if (promises.length === 0) {
                    subscription.complete({ reply: true });
                }
            });
        }
        for (const definition of subscriptions) {
            const operationName = (_b = definition.name) === null || _b === void 0 ? void 0 : _b.value;
            let result;
            try {
                result = (0, graphql_1.subscribe)({
                    rootValue: options.root,
                    contextValue,
                    variableValues,
                    document,
                    operationName,
                    schema: options.schema,
                });
            }
            catch (error) {
                result = Promise.reject(error);
            }
            promises.push(result);
            for (const promise of promises) {
                promise.then((result) => __awaiter(this, void 0, void 0, function* () {
                    var _c;
                    if ((_c = result.errors) === null || _c === void 0 ? void 0 : _c.length) {
                        this.send(connection, {
                            id: message.id,
                            type: common_1.GRAPHQL.subscribe_error,
                            errors: result.errors,
                        });
                        promises = promises.filter((p) => p !== promise);
                        if (promises.length === 0) {
                            subscription.complete({ reply: true });
                        }
                        return;
                    }
                    let race;
                    const canceled = subscription.cancel.promise();
                    const closed = connection.state.next();
                    do {
                        race = yield Promise.race([
                            canceled,
                            closed,
                            result.next(),
                        ]);
                        if (race.value) {
                            const value = race.value;
                            const data = value.data;
                            if (data) {
                                const response = {
                                    data: {},
                                };
                                for (const key of Object.keys(data)) {
                                    if (data[key] !== common_1.NO_RESPONSE) {
                                        response.data[key] = data[key];
                                    }
                                }
                                if (Object.keys(response.data).length) {
                                    this.send(connection, {
                                        id: message.id,
                                        type: common_1.GRAPHQL.subscribe_next,
                                        data: response.data,
                                    });
                                }
                            }
                        }
                    } while (race.value &&
                        !race.done);
                    yield result.return(null);
                    promises = promises.filter((p) => p !== promise);
                    if (promises.length === 0) {
                        if (race.reason === "canceled" ||
                            race.done) {
                            subscription.complete({ reply: true });
                        }
                        else {
                            subscription.complete({ reply: false });
                        }
                    }
                }));
                /*
                  .catch((error) => {
                    this.send(connection, {
                      id: message.id,
                      type: GRAPHQL.subscribe_error,
                      errors: [error.toString()],
                    });
                    promises = promises.filter((p) => p !== promise);
                    if (promises.length === 0) {
                      subscription.complete({ reply: true });
                    }
                  });
                  */
            }
        }
    }));
}
exports.initGraphQLServerHandlers = initGraphQLServerHandlers;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2hhbmRsZXIvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHFDQVVpQjtBQUVqQixzRUFRd0M7QUFFeEMsc0NBTW1CO0FBQ25CLG9DQUFzQztBQUV0QyxTQUFnQix5QkFBeUIsQ0FFdkMsT0FBNkI7SUFFN0IsSUFBSSxDQUFDLE1BQU0sQ0FDVCxnQkFBTyxDQUFDLGNBQWMsRUFDdEIsQ0FBQyxVQUE2QixFQUFFLE9BQU8sRUFBRSxFQUFFOztRQUN6QyxNQUFBLFVBQVUsQ0FBQyxhQUFjLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFZLENBQUMsMENBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pFLENBQUMsQ0FDRixDQUFDO0lBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FDVCxnQkFBTyxDQUFDLGdCQUFnQixFQUN4QixDQUFDLFVBQTZCLEVBQUUsT0FBTyxFQUFFLEVBQUU7O1FBQ3pDLE1BQUEsVUFBVTthQUNQLGFBQWMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQywwQ0FDdkMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO0lBQzVDLENBQUMsQ0FDRixDQUFDO0lBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FDVCxnQkFBTyxDQUFDLGVBQWUsRUFDdkIsQ0FBTyxVQUE2QixFQUFFLE9BQU8sRUFBRSxFQUFFOztRQUMvQyxJQUFJLFFBQXNCLENBQUM7UUFDM0IsSUFBSTtZQUNGLFFBQVEsR0FBRyxJQUFBLGVBQUssRUFBQyxPQUFPLENBQUMsS0FBZSxDQUFDLENBQUM7U0FDM0M7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLGdCQUFPLENBQUMsZUFBZTtnQkFDN0IsTUFBTSxFQUFFLENBQUUsS0FBc0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUM3QyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxnQkFBTyxDQUFDLGtCQUFrQjthQUNqQyxDQUFDLENBQUM7WUFDSCxPQUFPO1NBQ1I7UUFDRCxNQUFNLGdCQUFnQixHQUFHLElBQUEsa0JBQVEsRUFBQyxPQUFPLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzVELElBQUksZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLGdCQUFPLENBQUMsZUFBZTtnQkFDN0IsTUFBTSxFQUFFLGdCQUFnQjthQUN6QixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNkLElBQUksRUFBRSxnQkFBTyxDQUFDLGtCQUFrQjthQUNqQyxDQUFDLENBQUM7WUFDSCxPQUFPO1NBQ1I7UUFDRCxJQUFJLFlBQVksR0FBNEIsRUFBRSxDQUFDO1FBQy9DLE1BQU0sY0FBYyxHQUFHLE9BQU8sQ0FBQyxTQUFvQyxDQUFDO1FBQ3BFLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixJQUFJLElBQUEsa0JBQVUsRUFBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQy9CLDhDQUE4QztnQkFDOUMsTUFBTSxPQUFPLEdBQUcsTUFBTSxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLE9BQU8sRUFBRTtvQkFDWCxZQUFZLEdBQUcsT0FBTyxDQUFDO2lCQUN4QjthQUNGO1NBQ0Y7UUFDRCxZQUFZLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUM7UUFDcEMsWUFBWSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQWMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkUsWUFBWSxDQUFDLGNBQWMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQztRQUNyRSxNQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQ3pELE9BQVEsVUFBc0MsQ0FBQyxTQUFTLEtBQUssT0FBTyxDQUFDO1FBQ3ZFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUMzRCxPQUFRLFVBQXNDLENBQUMsU0FBUyxLQUFLLFVBQVUsQ0FBQztRQUMxRSxDQUFDLENBQUMsQ0FBQztRQUNILE1BQU0sYUFBYSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDL0QsT0FBTyxDQUNKLFVBQXNDLENBQUMsU0FBUyxLQUFLLGNBQWMsQ0FDckUsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDaEUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsZ0JBQU8sQ0FBQyxlQUFlO2dCQUM3QixNQUFNLEVBQUU7b0JBQ04sNERBQTREO2lCQUM3RDthQUNGLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxFQUFFLGdCQUFPLENBQUMsa0JBQWtCO2FBQ2pDLENBQUMsQ0FBQztZQUNILE9BQU87U0FDUjtRQUNELE1BQU0sWUFBWSxHQUFpQjtZQUNqQyxTQUFTLEVBQUUsSUFBSSwyQkFBTyxDQUFDLEdBQUcsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtvQkFDZCxJQUFJLEVBQUUsZ0JBQU8sQ0FBQyxjQUFjO2lCQUM3QixDQUFDLENBQUM7Z0JBQ0gsWUFBWSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsSUFBQSwwQkFBTSxFQUFDLENBQUMsRUFBRSw2QkFBUyxDQUFDLENBQUM7Z0JBQ3RELFlBQVksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkMsQ0FBQyxFQUFFLElBQUEsMEJBQU0sRUFBQyxDQUFDLEVBQUUsNkJBQVMsQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sRUFBRSxJQUFJLDJCQUFPLENBQUMsR0FBRyxFQUFFO2dCQUN4QixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM1QixZQUFZLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM5QixZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxVQUFVLENBQUMsYUFBYyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBWSxDQUFDLENBQUM7WUFDekQsQ0FBQyxFQUFFLDJCQUFPLENBQUM7WUFDWCxNQUFNLEVBQUUsSUFBSSw0QkFBUSxFQUFzQjtZQUMxQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUU7Z0JBQ3RCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzVCLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLElBQUksS0FBSyxFQUFFO29CQUNULElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO3dCQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxFQUFFLGdCQUFPLENBQUMsa0JBQWtCO3FCQUNqQyxDQUFDLENBQUM7aUJBQ0o7Z0JBQ0QsVUFBVSxDQUFDLGFBQWMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQVksQ0FBQyxDQUFDO1lBQ3pELENBQUM7U0FDRixDQUFDO1FBQ0YsVUFBVSxDQUFDLGFBQWMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQVksRUFBRSxZQUFZLENBQUMsQ0FBQztRQUNsRSxJQUFJLFFBQVEsR0FFUixFQUFFLENBQUM7UUFDUCxLQUFLLE1BQU0sVUFBVSxJQUFJLElBQUksS0FBSyxFQUFrQixDQUFDLE1BQU0sQ0FDekQsT0FBTyxFQUNQLFNBQVMsQ0FDVixFQUFFO1lBQ0QsTUFBTSxhQUFhLEdBQUcsTUFBQyxVQUFzQyxDQUFDLElBQUksMENBQzlELEtBQUssQ0FBQztZQUNWLElBQUksTUFBZSxDQUFDO1lBQ3BCLElBQUk7Z0JBQ0YsTUFBTSxHQUFHLElBQUEsaUJBQU8sRUFBQztvQkFDZixTQUFTLEVBQUUsT0FBTyxDQUFDLElBQUk7b0JBQ3ZCLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxRQUFRO29CQUNSLGFBQWE7b0JBQ2IsTUFBTSxFQUFFLE9BQU8sQ0FBQyxNQUFNO2lCQUN2QixDQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2hDO1lBQ0QsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN4QyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQW1DLENBQUMsQ0FBQztTQUNwRDtRQUNELEtBQUssTUFBTSxPQUFPLElBQUksUUFBUSxFQUFFO1lBQzlCLE9BQU87aUJBQ0osSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7O2dCQUNmLElBQUksTUFBQyxNQUEwQixDQUFDLE1BQU0sMENBQUUsTUFBTSxFQUFFO29CQUM5QyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTt3QkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO3dCQUNkLElBQUksRUFBRSxnQkFBTyxDQUFDLGVBQWU7d0JBQzdCLE1BQU0sRUFBRyxNQUEwQixDQUFDLE1BQU07cUJBQzNDLENBQUMsQ0FBQztpQkFDSjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTt3QkFDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO3dCQUNkLElBQUksRUFBRSxnQkFBTyxDQUFDLGNBQWM7d0JBQzVCLElBQUksRUFBRyxNQUEwQixDQUFDLElBQUk7cUJBQ3ZDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN6QixZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0gsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNwQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxFQUFFLGdCQUFPLENBQUMsZUFBZTtvQkFDN0IsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUMzQixDQUFDLENBQUM7Z0JBQ0gsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxPQUFPLENBQUMsQ0FBQztnQkFDakQsSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDekIsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2lCQUN4QztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxLQUFLLE1BQU0sVUFBVSxJQUFJLGFBQWEsRUFBRTtZQUN0QyxNQUFNLGFBQWEsR0FBRyxNQUFDLFVBQXNDLENBQUMsSUFBSSwwQ0FDOUQsS0FBSyxDQUFDO1lBQ1YsSUFBSSxNQUFlLENBQUM7WUFDcEIsSUFBSTtnQkFDRixNQUFNLEdBQUcsSUFBQSxtQkFBUyxFQUFDO29CQUNqQixTQUFTLEVBQUUsT0FBTyxDQUFDLElBQUk7b0JBQ3ZCLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxRQUFRO29CQUNSLGFBQWE7b0JBQ2IsTUFBTSxFQUFFLE9BQU8sQ0FBQyxNQUFNO2lCQUN2QixDQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2hDO1lBQ0QsUUFBUSxDQUFDLElBQUksQ0FDWCxNQUVDLENBQ0YsQ0FBQztZQUNGLEtBQUssTUFBTSxPQUFPLElBQUksUUFBUSxFQUFFO2dCQUM5QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQU8sTUFBTSxFQUFFLEVBQUU7O29CQUM1QixJQUFJLE1BQUMsTUFBMEIsQ0FBQyxNQUFNLDBDQUFFLE1BQU0sRUFBRTt3QkFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7NEJBQ3BCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTs0QkFDZCxJQUFJLEVBQUUsZ0JBQU8sQ0FBQyxlQUFlOzRCQUM3QixNQUFNLEVBQUcsTUFBMEIsQ0FBQyxNQUFNO3lCQUMzQyxDQUFDLENBQUM7d0JBQ0gsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxPQUFPLENBQUMsQ0FBQzt3QkFDakQsSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTs0QkFDekIsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3lCQUN4Qzt3QkFDRCxPQUFPO3FCQUNSO29CQUNELElBQUksSUFBYSxDQUFDO29CQUNsQixNQUFNLFFBQVEsR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUMvQyxNQUFNLE1BQU0sR0FBRyxVQUFVLENBQUMsS0FBTSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUN4QyxHQUFHO3dCQUNELElBQUksR0FBRyxNQUFNLE9BQU8sQ0FBQyxJQUFJLENBQUM7NEJBQ3hCLFFBQVE7NEJBQ1IsTUFBTTs0QkFDTCxNQUF5QixDQUFDLElBQUksRUFBRTt5QkFDbEMsQ0FBQyxDQUFDO3dCQUNILElBQUssSUFBeUMsQ0FBQyxLQUFLLEVBQUU7NEJBQ3BELE1BQU0sS0FBSyxHQUFJLElBQXlDLENBQUMsS0FBSyxDQUFDOzRCQUMvRCxNQUFNLElBQUksR0FBSSxLQUF5QixDQUFDLElBR3ZDLENBQUM7NEJBQ0YsSUFBSSxJQUFJLEVBQUU7Z0NBQ1IsTUFBTSxRQUFRLEdBQXNDO29DQUNsRCxJQUFJLEVBQUUsRUFBRTtpQ0FDVCxDQUFDO2dDQUNGLEtBQUssTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtvQ0FDbkMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssb0JBQVcsRUFBRTt3Q0FDN0IsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7cUNBQ2hDO2lDQUNGO2dDQUNELElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFO29DQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTt3Q0FDcEIsRUFBRSxFQUFFLE9BQU8sQ0FBQyxFQUFFO3dDQUNkLElBQUksRUFBRSxnQkFBTyxDQUFDLGNBQWM7d0NBQzVCLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSTtxQ0FDcEIsQ0FBQyxDQUFDO2lDQUNKOzZCQUNGO3lCQUNGO3FCQUNGLFFBQ0UsSUFBeUMsQ0FBQyxLQUFLO3dCQUNoRCxDQUFFLElBQXlDLENBQUMsSUFBSSxFQUNoRDtvQkFDRixNQUFPLE1BQXlCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM5QyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLE9BQU8sQ0FBQyxDQUFDO29CQUNqRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO3dCQUN6QixJQUNHLElBQTJCLENBQUMsTUFBTSxLQUFLLFVBQVU7NEJBQ2pELElBQXlDLENBQUMsSUFBSSxFQUMvQzs0QkFDQSxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7eUJBQ3hDOzZCQUFNOzRCQUNMLFlBQVksQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQzt5QkFDekM7cUJBQ0Y7Z0JBQ0gsQ0FBQyxDQUFBLENBQUMsQ0FBQztnQkFDSDs7Ozs7Ozs7Ozs7O29CQVlJO2FBQ0w7U0FDRjtJQUNILENBQUMsQ0FBQSxDQUNGLENBQUM7QUFDSixDQUFDO0FBclJELDhEQXFSQyJ9