"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NO_RESPONSE = exports.GRAPHQL = void 0;
var GRAPHQL;
(function (GRAPHQL) {
    GRAPHQL["subscribe_start"] = "subscribe_start";
    GRAPHQL["subscribe_next"] = "subscribe_next";
    GRAPHQL["subscribe_error"] = "subscribe_error";
    GRAPHQL["subscribe_cancel"] = "subscribe_cancel";
    GRAPHQL["subscribe_complete"] = "subscribe_complete";
    GRAPHQL["subscribe_ping"] = "subscribe_ping";
    GRAPHQL["subscribe_pong"] = "subscribe_pong";
})(GRAPHQL || (exports.GRAPHQL = GRAPHQL = {}));
exports.NO_RESPONSE = "#no-response";
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2NvbW1vbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFZQSxJQUFZLE9BUVg7QUFSRCxXQUFZLE9BQU87SUFDakIsOENBQW1DLENBQUE7SUFDbkMsNENBQWlDLENBQUE7SUFDakMsOENBQW1DLENBQUE7SUFDbkMsZ0RBQXFDLENBQUE7SUFDckMsb0RBQXlDLENBQUE7SUFDekMsNENBQWlDLENBQUE7SUFDakMsNENBQWlDLENBQUE7QUFDbkMsQ0FBQyxFQVJXLE9BQU8sdUJBQVAsT0FBTyxRQVFsQjtBQUVZLFFBQUEsV0FBVyxHQUFHLGNBQWMsQ0FBQyJ9