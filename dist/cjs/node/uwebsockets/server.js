"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeMicroGraphQLWebSocketServer = void 0;
const websocket_manager_1 = require("@napalmbrain/websocket-manager");
const server_1 = require("../../handler/server");
class NodeMicroGraphQLWebSocketServer extends websocket_manager_1.NodeMicroWebSocketServer {
    constructor(options) {
        super(options);
        server_1.initGraphQLServerHandlers.call(this, options);
    }
    // @ts-ignore ignore type error.
    init(connection) {
        if (!connection.subscriptions) {
            connection.subscriptions = new Map();
        }
        // @ts-ignore ignore type error.
        super.init(connection);
    }
    // @ts-ignore ignore type error.
    deinit(connection) {
        for (const subscription of connection.subscriptions.values()) {
            subscription.keepalive.stop();
            subscription.timeout.stop();
        }
        connection.subscriptions.clear();
        // @ts-ignore ignore type error.
        super.deinit(connection);
    }
}
exports.NodeMicroGraphQLWebSocketServer = NodeMicroGraphQLWebSocketServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvdXdlYnNvY2tldHMvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLHNFQUd3QztBQUV4QyxpREFBaUU7QUFRakUsTUFBYSwrQkFBZ0MsU0FBUSw0Q0FBd0I7SUFDM0UsWUFBWSxPQUFzRDtRQUNoRSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDZixrQ0FBeUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxnQ0FBZ0M7SUFDekIsSUFBSSxDQUFDLFVBQTZCO1FBQ3ZDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFO1lBQzdCLFVBQVUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxHQUFHLEVBQXdCLENBQUM7U0FDNUQ7UUFDRCxnQ0FBZ0M7UUFDaEMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQsZ0NBQWdDO0lBQ3pCLE1BQU0sQ0FBQyxVQUE2QjtRQUN6QyxLQUFLLE1BQU0sWUFBWSxJQUFJLFVBQVUsQ0FBQyxhQUFjLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDN0QsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzdCO1FBQ0QsVUFBVSxDQUFDLGFBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxnQ0FBZ0M7UUFDaEMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQixDQUFDO0NBQ0Y7QUF6QkQsMEVBeUJDIn0=