"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeGraphQLWebSocketClient = void 0;
const graphql_1 = require("graphql");
const websocket_manager_1 = require("@napalmbrain/websocket-manager");
const client_1 = require("../../handler/client");
const common_1 = require("../../common");
const utils_1 = require("../../utils");
class NodeGraphQLWebSocketClient extends websocket_manager_1.NodeWebSocketClient {
    constructor(options = {}) {
        super(options);
        client_1.initGraphQLClientHandlers.call(this);
    }
    cancel(connection, action, wait = false) {
        if (!wait) {
            action.observable.complete();
        }
        this.send(connection, {
            id: action.message.id,
            type: common_1.GRAPHQL.subscribe_cancel,
        });
    }
    subscribe(connection, operation, observers = [], retry = -1) {
        let query = operation.query;
        if (!(0, utils_1.isString)(query)) {
            // @ts-ignore: `query` should be printable.
            query = (0, graphql_1.print)(query);
        }
        return this.message(connection, Object.assign(Object.assign({}, operation), { query, type: common_1.GRAPHQL.subscribe_start }), observers, retry);
    }
}
exports.NodeGraphQLWebSocketClient = NodeGraphQLWebSocketClient;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3MvY2xpZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLHFDQUFnQztBQUNoQyxzRUFBcUU7QUFXckUsaURBQWlFO0FBQ2pFLHlDQUF5RDtBQUN6RCx1Q0FBdUM7QUFFdkMsTUFBYSwwQkFBMkIsU0FBUSx1Q0FBbUI7SUFDakUsWUFBWSxVQUFrQyxFQUFFO1FBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNmLGtDQUF5QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQXNCLEVBQUUsTUFBdUIsRUFBRSxJQUFJLEdBQUcsS0FBSztRQUN6RSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3BCLEVBQUUsRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDckIsSUFBSSxFQUFFLGdCQUFPLENBQUMsZ0JBQWdCO1NBQy9CLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTSxTQUFTLENBQ2QsVUFBc0IsRUFDdEIsU0FBMkIsRUFDM0IsWUFBaUMsRUFBRSxFQUNuQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBRVYsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsSUFBQSxnQkFBUSxFQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLDJDQUEyQztZQUMzQyxLQUFLLEdBQUcsSUFBQSxlQUFLLEVBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEI7UUFDRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQ2pCLFVBQVUsa0NBRUwsU0FBUyxLQUNaLEtBQUssRUFDTCxJQUFJLEVBQUUsZ0JBQU8sQ0FBQyxlQUFlLEtBRS9CLFNBQVMsRUFDVCxLQUFLLENBQ04sQ0FBQztJQUNKLENBQUM7Q0FDRjtBQXRDRCxnRUFzQ0MifQ==