"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeGraphQLWebSocketServer = void 0;
const websocket_manager_1 = require("@napalmbrain/websocket-manager");
const server_1 = require("../../handler/server");
class NodeGraphQLWebSocketServer extends websocket_manager_1.NodeWebSocketServer {
    constructor(options) {
        super();
        server_1.initGraphQLServerHandlers.call(this, options);
    }
    init(connection) {
        if (!connection.subscriptions) {
            connection.subscriptions = new Map();
        }
        super.init(connection);
    }
    deinit(connection) {
        for (const subscription of connection.subscriptions.values()) {
            subscription.keepalive.stop();
            subscription.timeout.stop();
        }
        connection.subscriptions.clear();
        super.deinit(connection);
    }
}
exports.NodeGraphQLWebSocketServer = NodeGraphQLWebSocketServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL25vZGUvd3Mvc2VydmVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLHNFQUd3QztBQUV4QyxpREFBaUU7QUFPakUsTUFBYSwwQkFBMkIsU0FBUSx1Q0FBbUI7SUFDakUsWUFBWSxPQUFzRDtRQUNoRSxLQUFLLEVBQUUsQ0FBQztRQUNSLGtDQUF5QixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVNLElBQUksQ0FBQyxVQUE2QjtRQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRTtZQUM3QixVQUFVLENBQUMsYUFBYSxHQUFHLElBQUksR0FBRyxFQUF3QixDQUFDO1NBQzVEO1FBQ0QsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQTZCO1FBQ3pDLEtBQUssTUFBTSxZQUFZLElBQUksVUFBVSxDQUFDLGFBQWMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUM3RCxZQUFZLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzlCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDN0I7UUFDRCxVQUFVLENBQUMsYUFBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2xDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDM0IsQ0FBQztDQUNGO0FBckJELGdFQXFCQyJ9