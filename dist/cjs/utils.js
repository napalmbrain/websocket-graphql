"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAsyncGenerator = exports.isFunction = exports.isString = void 0;
function isString(value) {
    if (typeof value === "string") {
        return true;
    }
    return false;
}
exports.isString = isString;
function isFunction(value) {
    if (typeof value === "function") {
        return true;
    }
    return false;
}
exports.isFunction = isFunction;
function isAsyncGenerator(object) {
    if (Object.hasOwn(object, Symbol.asyncIterator)) {
        return true;
    }
    return false;
}
exports.isAsyncGenerator = isAsyncGenerator;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsU0FBZ0IsUUFBUSxDQUFDLEtBQWM7SUFDckMsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7UUFDN0IsT0FBTyxJQUFJLENBQUM7S0FDYjtJQUNELE9BQU8sS0FBSyxDQUFDO0FBQ2YsQ0FBQztBQUxELDRCQUtDO0FBRUQsU0FBZ0IsVUFBVSxDQUFDLEtBQWM7SUFDdkMsSUFBSSxPQUFPLEtBQUssS0FBSyxVQUFVLEVBQUU7UUFDL0IsT0FBTyxJQUFJLENBQUM7S0FDYjtJQUNELE9BQU8sS0FBSyxDQUFDO0FBQ2YsQ0FBQztBQUxELGdDQUtDO0FBRUQsU0FBZ0IsZ0JBQWdCLENBQUMsTUFBYztJQUM3QyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUMsRUFBRTtRQUMvQyxPQUFPLElBQUksQ0FBQztLQUNiO0lBQ0QsT0FBTyxLQUFLLENBQUM7QUFDZixDQUFDO0FBTEQsNENBS0MifQ==