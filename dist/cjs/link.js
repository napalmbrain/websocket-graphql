"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApolloWebSocketLink = void 0;
const core_1 = require("@apollo/client/core");
const client_1 = require("./client");
class ApolloWebSocketLink extends core_1.ApolloLink {
    constructor(url, options) {
        super();
        this.url = url;
        this.client = new client_1.GraphQLWebSocketClient(options);
        this.connection = this.client.connect(this.url);
    }
    request(operation, _forward) {
        return new core_1.Observable((observer) => {
            const action = this.client.subscribe(this.connection, 
            // deno-lint-ignore no-explicit-any
            operation, [observer], -1);
            return () => this.client.cancel(this.connection, action);
        });
    }
}
exports.ApolloWebSocketLink = ApolloWebSocketLink;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGluay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9saW5rLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDhDQU02QjtBQU83QixxQ0FBa0Q7QUFFbEQsTUFBYSxtQkFBb0IsU0FBUSxpQkFBVTtJQUtqRCxZQUFZLEdBQWlCLEVBQUUsT0FBZ0M7UUFDN0QsS0FBSyxFQUFFLENBQUM7UUFDUixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSwrQkFBc0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRU0sT0FBTyxDQUNaLFNBQW9CLEVBQ3BCLFFBQStCO1FBRS9CLE9BQU8sSUFBSSxpQkFBVSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDakMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQ2xDLElBQUksQ0FBQyxVQUFVO1lBQ2YsbUNBQW1DO1lBQ25DLFNBQWdCLEVBQ2hCLENBQUMsUUFBUSxDQUFDLEVBQ1YsQ0FBQyxDQUFDLENBQ0gsQ0FBQztZQUNGLE9BQU8sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMzRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0FDRjtBQTNCRCxrREEyQkMifQ==