export function isString(value: unknown): boolean {
  if (typeof value === "string") {
    return true;
  }
  return false;
}

export function isFunction(value: unknown): boolean {
  if (typeof value === "function") {
    return true;
  }
  return false;
}

export function isAsyncGenerator(object: object) {
  if (Object.hasOwn(object, Symbol.asyncIterator)) {
    return true;
  }
  return false;
}
