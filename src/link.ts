import {
  ApolloLink,
  Operation,
  FetchResult,
  Observable,
  NextLink,
} from "https://esm.sh/v131/@apollo/client@3.8.1/core";

import {
  Connection,
  WebSocketClientOptions,
} from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/mod.ts";

import { GraphQLWebSocketClient } from "./client.ts";

export class ApolloWebSocketLink extends ApolloLink {
  private url: string | URL;
  private client: GraphQLWebSocketClient;
  private connection: Connection;

  constructor(url: string | URL, options?: WebSocketClientOptions) {
    super();
    this.url = url;
    this.client = new GraphQLWebSocketClient(options);
    this.connection = this.client.connect(this.url);
  }

  public request(
    operation: Operation,
    _forward?: NextLink | undefined
  ): Observable<FetchResult> | null {
    return new Observable((observer) => {
      const action = this.client.subscribe(
        this.connection,
        // deno-lint-ignore no-explicit-any
        operation as any,
        [observer],
        -1
      );
      return () => this.client.cancel(this.connection, action);
    });
  }
}
