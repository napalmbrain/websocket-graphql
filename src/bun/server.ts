import { BunWebSocketServer } from "@napalmbrain/websocket-manager/dist/src/bun/server.ts";
import { WebSocketServerOptions } from "@napalmbrain/websocket-manager";

import { initGraphQLServerHandlers } from "../handler/server.ts";
import {
  GraphQLServerOptions,
  GraphQLConnection,
  Subscription,
} from "../common.ts";

export class BunGraphQLWebSocketServer extends BunWebSocketServer {
  constructor(options: WebSocketServerOptions & GraphQLServerOptions) {
    // @ts-ignore ignore type error.
    super(options);
    // @ts-ignore ignore type error.
    initGraphQLServerHandlers.call(this, options);
  }

  // @ts-ignore ignore type error.
  public init(connection: GraphQLConnection) {
    if (!connection.subscriptions) {
      connection.subscriptions = new Map<string, Subscription>();
    }
    // @ts-ignore ignore type error.
    super.init(connection);
  }

  // @ts-ignore ignore type error.
  public deinit(connection: GraphQLConnection) {
    for (const subscription of connection.subscriptions!.values()) {
      subscription.keepalive.stop();
      subscription.timeout.stop();
    }
    connection.subscriptions!.clear();
    // @ts-ignore ignore type error.
    super.deinit(connection);
  }
}
