import { print } from "graphql";
import { NodeWebSocketClient } from "@napalmbrain/websocket-manager";

import {
  Action,
  Connection,
  Message,
  Observer,
  WebSocketClientOptions,
  //} from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/mod.ts";
} from "@napalmbrain/websocket-manager";

import { initGraphQLClientHandlers } from "../../handler/client.ts";
import { GRAPHQL, GraphQLOperation } from "../../common.ts";
import { isString } from "../../utils.ts";

export class NodeGraphQLWebSocketClient extends NodeWebSocketClient {
  constructor(options: WebSocketClientOptions = {}) {
    super(options);
    initGraphQLClientHandlers.call(this);
  }

  public cancel(connection: Connection, action: Action<Message>, wait = false) {
    if (!wait) {
      action.observable.complete();
    }
    this.send(connection, {
      id: action.message.id,
      type: GRAPHQL.subscribe_cancel,
    });
  }

  public subscribe(
    connection: Connection,
    operation: GraphQLOperation,
    observers: Observer<Message>[] = [],
    retry = -1
  ): Action<Message> {
    let query = operation.query;
    if (!isString(query)) {
      // @ts-ignore: `query` should be printable.
      query = print(query);
    }
    return this.message(
      connection,
      {
        ...operation,
        query,
        type: GRAPHQL.subscribe_start,
      },
      observers,
      retry
    );
  }
}
