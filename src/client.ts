import { print } from "https://esm.sh/v131/graphql@16.8.0";

import {
  Action,
  Connection,
  Message,
  Observer,
  WebSocketClient,
  WebSocketClientOptions,
  TIMEOUT,
} from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/mod.ts";

import { GRAPHQL, GraphQLOperation } from "./common.ts";
import { initGraphQLClientHandlers } from "./handler/client.ts";
import { isString } from "./utils.ts";

export class GraphQLWebSocketClient extends WebSocketClient {
  constructor(options: WebSocketClientOptions = {}) {
    super(options);
    initGraphQLClientHandlers.call(this);
  }

  public cancel(connection: Connection, action: Action<Message>, wait = false) {
    if (!wait) {
      action.observable.complete();
    }
    this.send(connection, {
      id: action.message.id,
      type: GRAPHQL.subscribe_cancel,
    });
  }

  public subscribe(
    connection: Connection,
    operation: GraphQLOperation,
    observers: Observer<Message>[] = [],
    timeout = -1
  ): Action<Message> {
    let query = operation.query;
    if (!isString(query)) {
      // @ts-ignore: `query` should be printable.
      query = print(query);
    }
    return this.message(
      connection,
      {
        ...operation,
        query,
        type: GRAPHQL.subscribe_start,
      },
      observers,
      timeout
    );
  }
}
