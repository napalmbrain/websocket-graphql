import {
  DocumentNode,
  GraphQLSchema,
} from "https://esm.sh/v131/graphql@16.8.0";

import {
  Connection,
  Timeout,
  Signaler,
  Generic,
} from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/mod.ts";

export enum GRAPHQL {
  subscribe_start = "subscribe_start",
  subscribe_next = "subscribe_next",
  subscribe_error = "subscribe_error",
  subscribe_cancel = "subscribe_cancel",
  subscribe_complete = "subscribe_complete",
  subscribe_ping = "subscribe_ping",
  subscribe_pong = "subscribe_pong",
}

export const NO_RESPONSE = "#no-response";

export interface Subscription {
  keepalive: Timeout;
  timeout: Timeout;
  cancel: Signaler<{ reason: string }>;
  complete: (params: { reply: boolean }) => void;
}

export interface GraphQLConnection extends Connection {
  subscriptions?: Map<string, Subscription>;
}

export interface GraphQLOperation {
  query: string | DocumentNode;
  variables?: Record<string, unknown>;
}

type Context =
  | ((context: Generic) => Generic)
  | ((context: Generic) => Promise<Generic>);

export type Resolver = (
  parent: void | Generic,
  args: Generic,
  context: {
    [key: string]:
      | Generic
      | (() => number)
      | ((meta: Generic) => Promise<Generic>);
    meta: Generic;
    setMeta: (meta: Generic) => Promise<Generic>;
    bufferedAmount: () => number;
  },
  info: Generic
  // deno-lint-ignore no-explicit-any
) => any | Promise<any>;

export type Subscriber = {
  subscribe: Resolver;
};

export interface Resolvers {
  [key: string]: {
    [key: string]: Resolver | Subscriber;
  };
}

export interface GraphQLServerOptions {
  schema: GraphQLSchema;
  context?: Context;
  root?: unknown;
}
