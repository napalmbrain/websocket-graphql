import {
  DenoWebSocketServer,
  WebSocketServerOptions,
} from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/mod.ts";
import { initGraphQLServerHandlers } from "../handler/server.ts";
import {
  GraphQLServerOptions,
  GraphQLConnection,
  Subscription,
} from "../common.ts";

export class DenoGraphQLWebSocketServer extends DenoWebSocketServer {
  constructor(options: WebSocketServerOptions & GraphQLServerOptions) {
    super(options);
    initGraphQLServerHandlers.call(this, options);
  }

  public init(connection: GraphQLConnection) {
    if (!connection.subscriptions) {
      connection.subscriptions = new Map<string, Subscription>();
    }
    super.init(connection);
  }

  public deinit(connection: GraphQLConnection) {
    for (const subscription of connection.subscriptions!.values()) {
      subscription.keepalive.stop();
      subscription.timeout.stop();
    }
    connection.subscriptions!.clear();
    super.deinit(connection);
  }
}
