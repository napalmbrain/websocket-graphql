function strip-typescript-extensions -d "Remove .ts from typescript imports."
  set -l file $argv[1]
  sed -r -i '' 's/"(.*).ts";/"\1";/g' $file
end

function strip-typescript-extensions-recursive -d "Recursively remove .ts from typescript imports."
  set -l directory $argv[1]
  for item in (ls $directory)
    if test -d $directory/$item
      strip-typescript-extensions-recursive $directory/$item
    else if test -f $directory/$item
      strip-typescript-extensions $directory/$item
    end
  end
end

set -l directory $argv[1]

if test -z $directory
  echo "Please provide a directory as the first argument to this script."
  exit 1
end

strip-typescript-extensions-recursive $directory