function remap-typescript-imports -d "Remap typescript imports using the 'import-remap' file."
  for line in (cat ./import-remap)
    set -l split (string split '#' $line)
    set -l specifier $split[1]
    set -l replacement $split[2]
    sed -r -i '' "s#$specifier#$replacement#g" $argv[1]
  end
end

function remap-typescript-imports-recursive -d "Recursively remap typescript imports using the 'import-remap' file."
  set -l directory $argv[1]
  for item in (ls $directory)
    if test -d $directory/$item
      remap-typescript-imports-recursive $directory/$item
    else if test -f $directory/$item
      remap-typescript-imports $directory/$item
    end
  end
end

set -l directory $argv[1]

if test -z $directory
  echo "Please provide a directory as the first argument to this script."
  exit 1
end

remap-typescript-imports-recursive $directory