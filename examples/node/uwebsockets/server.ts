import { GraphQLError } from "graphql";
import { makeExecutableSchema } from "@graphql-tools/schema";
import { gql } from "graphql-tag";
import { App } from "uWebSockets.js";

import { NodeMicroGraphQLWebSocketServer } from "./../../../dist/src/node/uwebsockets/server";

const typeDefs = gql`
  type Query {
    hello: String
  }

  type Subscription {
    hi: String
    once: String
  }
`;

const resolvers = {
  Query: {
    hello: () => `Hello world!`,
  },
  Subscription: {
    hi: {
      async *subscribe() {
        while (true) {
          //throw new GraphQLError("Test");
          yield { hi: "hi" };
          await new Promise<void>((resolve) => {
            setTimeout(resolve, 1000);
          });
        }
      },
    },
    once: {
      async *subscribe() {
        yield { once: "once" };
      },
    },
  },
};

const schema = makeExecutableSchema({ typeDefs, resolvers });

const server = new NodeMicroGraphQLWebSocketServer({
  app: App(),
  schema,
});

server.listen();
