import { gql } from "graphql-tag";
import { NodeGraphQLWebSocketClient } from "./../../../dist/src/node/ws/client";

const client = new NodeGraphQLWebSocketClient();
const connection = client.connect(new URL("ws://localhost:8000/graphql"));

/*
client.subscribe(
  connection,
  {
    query: gql`
      subscription {
        once
      }
    `,
  },
  [
    {
      next: (message) => {
        console.log("NEXT: ", message);
      },
      error: (message) => {
        console.log("ERROR: ", message);
      },
      complete: () => {
        console.log("SUBSCRIBE ONCE COMPLETE");
      },
    },
  ]
);

client.subscribe(
  connection,
  {
    query: gql`
      subscription {
        hi
      }
    `,
  },
  [
    {
      next: (message) => {
        console.log("NEXT: ", message);
      },
      error: (message) => {
        console.log("ERROR: ", message);
      },
      complete: () => {
        console.log("SUBSCRIBE HI COMPLETE");
      },
    },
  ]
);
*/

client.subscribe(
  connection,
  {
    query: gql`
      subscription Once {
        once
      }
      subscription Hi {
        hi
      }
    `,
  },
  [
    {
      next: (message) => {
        console.log("NEXT: ", message);
      },
      error: (message) => {
        console.log("ERROR: ", message);
      },
      complete: () => {
        console.log("SUBSCRIBE COMPLETE");
      },
    },
  ]
);
