import { GraphQLError } from "graphql";
import { makeExecutableSchema } from "@graphql-tools/schema";
import { gql } from "graphql-tag";

import { NodeGraphQLWebSocketServer } from "./../../../dist/src/node/ws/server";

const typeDefs = gql`
  type Query {
    hello: String
  }

  type Subscription {
    hi: String
    once: String
  }
`;

const resolvers = {
  Query: {
    hello: () => `Hello world!`,
  },
  Subscription: {
    hi: {
      async *subscribe() {
        while (true) {
          //throw new GraphQLError("Test");
          await new Promise<void>((resolve) => {
            setTimeout(resolve, 1000);
          });
          yield { hi: "hi" };
        }
      },
    },
    once: {
      async *subscribe() {
        yield { once: "once" };
      },
    },
  },
};

const schema = makeExecutableSchema({ typeDefs, resolvers });

const server = new NodeGraphQLWebSocketServer({
  schema,
});

server.listen();
