import { makeExecutableSchema } from "@graphql-tools/schema";
import { gql } from "graphql-tag";

import { BunGraphQLWebSocketServer } from "./../../dist/src/bun/server.ts";
import { Resolvers } from "../../dist/src/common.ts";

const typeDefs = gql`
  type Query {
    hello: String
  }

  type Subscription {
    hi: String
    once: String
  }
`;

const resolvers: Resolvers = {
  Query: {
    hello: () => `Hello world!`,
  },
  Subscription: {
    hi: {
      async *subscribe() {
        while (true) {
          //throw new GraphQLError("Test");
          yield { hi: "hi" };
          await new Promise<void>((resolve) => {
            setTimeout(resolve, 1000);
          });
        }
      },
    },
    once: {
      async *subscribe() {
        yield { once: "once" };
      },
    },
  },
};

const schema = makeExecutableSchema({ typeDefs, resolvers });

const server = new BunGraphQLWebSocketServer({
  schema,
});

server.options.callbacks!.onConnected = () => {
  console.log("CONNECTED");
};

server.options.callbacks!.onDisconnected = () => {
  console.log("DISCONNECTED");
};

server.listen();
