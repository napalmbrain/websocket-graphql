import { gql } from "https://esm.sh/v131/graphql-tag@2.12.6";
import { GraphQLWebSocketClient } from "./../../src/client.ts";

const client = new GraphQLWebSocketClient();
const connection = client.connect(new URL("ws://localhost:8000/graphql"));

/*
client.subscribe(
  connection,
  {
    query: gql`
      subscription {
        once
      }
    `,
  },
  [
    {
      next: (message) => {
        console.log("NEXT: ", message);
      },
      error: (message) => {
        console.log("ERROR: ", message);
      },
      complete: () => {
        console.log("SUBSCRIBE ONCE COMPLETE");
      },
    },
  ]
);

client.subscribe(
  connection,
  {
    query: gql`
      subscription {
        hi
      }
    `,
  },
  [
    {
      next: (message) => {
        console.log("NEXT: ", message);
      },
      error: (message) => {
        console.log("ERROR: ", message);
      },
      complete: () => {
        console.log("SUBSCRIBE HI COMPLETE");
      },
    },
  ]
);
*/

client.options.callbacks!.onConnected = () => {
  console.log("CONNECTED");
};

client.options.callbacks!.onDisconnected = () => {
  console.log("DISCONNECTED");
};

client.subscribe(
  connection,
  {
    query: gql`
      subscription Once {
        once
      }
      subscription Hi {
        hi
      }
    `,
  },
  [
    {
      next: (message) => {
        console.log("NEXT: ", message);
      },
      error: (message) => {
        console.log("ERROR: ", message);
      },
      complete: () => {
        console.log("SUBSCRIBE COMPLETE");
      },
    },
  ],
  false
);
