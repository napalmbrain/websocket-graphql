import { GraphQLError } from "https://esm.sh/v131/graphql@16.8.0";
import { makeExecutableSchema } from "https://esm.sh/v131/@graphql-tools/schema@10.0.0";
import { gql } from "https://esm.sh/v131/graphql-tag@2.12.6";

import { DenoGraphQLWebSocketServer } from "./../../src/deno/server.ts";
import { Resolvers } from "../../src/common.ts";

const typeDefs = gql`
  type Query {
    hello: String
  }

  type Subscription {
    hi: String
    once: String
  }
`;

const resolvers: Resolvers = {
  Query: {
    hello: () => `Hello world!`,
  },
  Subscription: {
    hi: {
      async *subscribe() {
        while (true) {
          //throw new GraphQLError("Test");
          yield { hi: "hi" };
          await new Promise<void>((resolve) => {
            setTimeout(resolve, 1000);
          });
        }
      },
    },
    once: {
      async *subscribe() {
        yield { once: "once" };
      },
    },
  },
};

const schema = makeExecutableSchema({ typeDefs, resolvers });

const server = new DenoGraphQLWebSocketServer({
  schema,
});

server.options.callbacks!.onConnected = () => {
  console.log("CONNECTED");
};

server.options.callbacks!.onDisconnected = () => {
  console.log("DISCONNECTED");
};

server.listen();
