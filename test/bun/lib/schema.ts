import { makeExecutableSchema } from "@graphql-tools/schema";
import { gql } from "graphql-tag";

import { NO_RESPONSE } from "../../../dist/src/common.ts";

const typeDefs = gql`
  type Query {
    hello: String
    there: String
  }

  type Mutation {
    change: String
    something: String
  }

  type Subscription {
    hi: String
    once: String
    twice: String
    thrice: String
  }
`;

const resolvers = {
  Query: {
    hello: () => "hello",
    there: () => "there",
  },
  Mutation: {
    change: () => "change",
    something: () => "something",
  },
  Subscription: {
    hi: {
      async *subscribe() {
        while (true) {
          yield { hi: "hi" };
          await new Promise<void>((resolve) => {
            setTimeout(resolve, 100);
          });
        }
      },
    },
    once: {
      async *subscribe() {
        yield { once: "once" };
      },
    },
    twice: {
      async *subscribe() {
        yield { twice: NO_RESPONSE };
        yield { twice: "twice" };
      },
    },
    thrice: {
      subscribe() {
        return {
          [Symbol.asyncIterator]() {
            let count = 0;
            return {
              next() {
                count++;
                if (count === 1) {
                  return { value: { thrice: NO_RESPONSE }, done: false };
                } else if (count === 2) {
                  return { value: { thrice: "thrice" }, done: false };
                } else {
                  return { done: true };
                }
              },
            };
          },
        };
      },
    },
  },
};

export const schema = makeExecutableSchema({ typeDefs, resolvers });
