import { describe, it, beforeEach, afterEach, expect, mock } from "bun:test";

import {
  Action,
  ConnectionAsyncIterator,
  Connection,
  Message,
  Generic,
} from "@napalmbrain/websocket-manager";

import { gql } from "graphql-tag";

import { NodeGraphQLWebSocketClient } from "../../dist/src/node/ws/client";
import { BunGraphQLWebSocketServer } from "../../dist/src/bun/server";
import { GraphQLConnection } from "../../dist/src/common";

import { schema } from "./lib/schema";

describe("handler", () => {
  const hostname = "localhost";
  const port = 8001;
  const url = new URL(`ws://${hostname}:${port}`);
  let server: BunGraphQLWebSocketServer;
  let client: NodeGraphQLWebSocketClient;
  let serverConnection: GraphQLConnection;
  let clientConnection: Connection;
  let serverIterator: ConnectionAsyncIterator;
  let clientIterator: ConnectionAsyncIterator;
  const connected = async (): Promise<Connection[]> => {
    return (
      await Promise.all([serverIterator.next(), clientIterator.next()])
    ).map(({ value }) => value) as Connection[];
  };

  beforeEach(async () => {
    server = new BunGraphQLWebSocketServer({ schema });
    server.listen({ hostname, port });
    client = new NodeGraphQLWebSocketClient({ reconnect: false });
    serverIterator = new ConnectionAsyncIterator(server);
    clientIterator = new ConnectionAsyncIterator(client);
    client.connect(url);
    [serverConnection, clientConnection] = await connected();
  });

  afterEach(async () => {
    await serverIterator.return();
    await clientIterator.return();
    await client.stop();
    await server.stop();
  });

  it("can query", async () => {
    const result: Generic = await new Promise((resolve) => {
      let r: Generic;
      client.subscribe(
        clientConnection,
        {
          query: gql`
            query {
              hello
            }
          `,
        },
        [
          {
            next: (message) => {
              r = message.data as Generic;
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ hello: "hello" });
  });

  it("can batch query", async () => {
    const result: Generic = await new Promise((resolve) => {
      const r: Generic = {};
      client.subscribe(
        clientConnection,
        {
          query: gql`
            query Hello {
              hello
            }
            query There {
              there
            }
          `,
        },
        [
          {
            next: (message) => {
              Object.assign(r, message.data);
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ hello: "hello", there: "there" });
  });

  it("can mutate", async () => {
    const result: Generic = await new Promise((resolve) => {
      let r: Generic;
      client.subscribe(
        clientConnection,
        {
          query: gql`
            mutation {
              change
            }
          `,
        },
        [
          {
            next: (message) => {
              r = message.data as Generic;
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ change: "change" });
  });

  it("can batch mutate", async () => {
    const result: Generic = await new Promise((resolve) => {
      const r: Generic = {};
      client.subscribe(
        clientConnection,
        {
          query: gql`
            mutation Change {
              change
            }
            mutation Something {
              something
            }
          `,
        },
        [
          {
            next: (message) => {
              Object.assign(r, message.data);
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ change: "change", something: "something" });
  });

  it("can subscribe", async () => {
    const result: Generic = await new Promise((resolve) => {
      let r: Generic;
      client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription {
              once
            }
          `,
        },
        [
          {
            next: (message) => {
              r = message.data as Generic;
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ once: "once" });
  });

  it("can skip a response as an async generator with `NO_RESPONSE`", async () => {
    let data;
    const next = mock((message: Message) => {
      data = message.data;
    });
    await new Promise<void>((resolve) => {
      client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription {
              twice
            }
          `,
        },
        [
          {
            next,
            complete: () => {
              resolve();
            },
          },
        ]
      );
    });
    expect(next).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ twice: "twice" });
  });

  it("can skip a response as an async iterator with `NO_RESPONSE`", async () => {
    let data;
    const next = mock((message: Message) => {
      data = message.data;
    });
    await new Promise<void>((resolve) => {
      client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription {
              thrice
            }
          `,
        },
        [
          {
            next,
            complete: () => {
              resolve();
            },
          },
        ]
      );
    });
    expect(next).toHaveBeenCalledTimes(1);
    expect(data).toEqual({ thrice: "thrice" });
  });

  it("can cancel a subscription", async () => {
    let id: string;
    let m: Message;
    let action: Action<Message>;
    const result: Generic = await new Promise<Generic>((resolve) => {
      action = client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription {
              hi
            }
          `,
        },
        [
          {
            next: (message) => {
              id = message.id as string;
              m = message;
              client.cancel(clientConnection, action, true);
            },
            complete: () => {
              resolve(m.data as Generic);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ hi: "hi" });
    // @ts-ignore: `id` is be defined.
    expect(serverConnection.subscriptions?.get(id)).toBeUndefined();
  });

  it("can batch subscribe", async () => {
    const result: Generic = await new Promise((resolve) => {
      let count = 0;
      const r: Generic = {};
      const action = client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription Hi {
              hi
            }
            subscription Once {
              once
            }
          `,
        },
        [
          {
            next: (message) => {
              Object.assign(r, message.data);
              count++;
              if (count >= 2) {
                client.cancel(clientConnection, action, true);
              }
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ hi: "hi", once: "once" });
  });

  it("can skip a response in a batch subscription as an async generator with `NO_RESPONSE`", async () => {
    const result: Generic = {};
    const next = mock((message: Message) => {
      Object.assign(result, message.data);
    });
    await new Promise<void>((resolve) => {
      client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription Once {
              once
            }
            subscription Twice {
              twice
            }
          `,
        },
        [
          {
            next,
            complete: () => {
              resolve();
            },
          },
        ]
      );
    });
    expect(next).toHaveBeenCalledTimes(2);
    expect(result).toEqual({ once: "once", twice: "twice" });
  });

  it("can skip a response in a batch subscription as an async iterator with `NO_RESPONSE`", async () => {
    const result: Generic = {};
    const next = mock((message: Message) => {
      Object.assign(result, message.data);
    });
    await new Promise<void>((resolve) => {
      client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription Once {
              once
            }
            subscription Thrice {
              thrice
            }
          `,
        },
        [
          {
            next,
            complete: () => {
              resolve();
            },
          },
        ]
      );
    });
    expect(next).toHaveBeenCalledTimes(2);
    expect(result).toEqual({ once: "once", thrice: "thrice" });
  });

  it("can cancel a batch subscription", async () => {
    let id: string;
    let action: Action<Message>;
    const result: Generic = await new Promise<Generic>((resolve) => {
      let count = 0;
      const r: Generic = {};
      action = client.subscribe(
        clientConnection,
        {
          query: gql`
            subscription Hi {
              hi
            }
            subscription Once {
              once
            }
          `,
        },
        [
          {
            next: (message) => {
              id = message.id as string;
              Object.assign(r, message.data);
              count++;
              if (count >= 2) {
                client.cancel(clientConnection, action, true);
              }
            },
            complete: () => {
              resolve(r);
            },
          },
        ]
      );
    });
    expect(result).toEqual({ hi: "hi", once: "once" });
    // @ts-ignore: `id` is be defined.
    expect(serverConnection.subscriptions?.get(id)).toBeUndefined();
  });
});
